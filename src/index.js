import ReactDOM from "react-dom/client";
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import App from "./App";
import LandingPage from "./routes/LandingPage";
import ProductByCategory from "./routes/ProductByCategory";
import ProductDetail from "./routes/ProductDetail";
import Cart from "./routes/Cart";
import Checkout from "./routes/Checkout";
import Login from "./routes/Login";
import Register from "./routes/Register";
import Invoice from "./routes/Invoice";
import Search from "./routes/Search";
import ScrollToTop from "./components/ScrollToTop";
import ManageProducts from "./routes/ManageProducts";
import ManageInvoices from "./routes/ManageInvoices";
import ManageBrands from "./routes/ManageBrands";
import Validation from "./routes/Validation";
import Base64Image from "./routes/Base64Image";
import InvoiceCheck from "./routes/InvoiceCheck";
import MobileCartButton from "./components/MobileCartButton";

const root = ReactDOM.createRoot(
  document.getElementById("root")
);
root.render(
  <BrowserRouter>
      <ScrollToTop />
    <Routes>

    <Route path="login" element={<Login />} />
    <Route path="pilihan" element={<Pilihan />} />
    <Route path="loginadmin" element={<LoginAdmin />} />
    <Route path="validation" element={<Validation />} />
    <Route path="register" element={<Register />} />
    <Route path="validation/:verifToken" element={<Validation />} />

      <Route path="/" element={<div><App /><MobileCartButton /></div>}>
          <Route path="" element={<LandingPage />} />
          <Route path="category" ><Route path=":categoryId" element={<ProductByCategory />} /></Route>
          <Route path="detail" ><Route path=":productId" element={<ProductDetail />} /></Route>
          <Route path="cart" element={<Cart />} />
          <Route path="checkout" element={<Checkout />} />
          <Route path="invoice" element={<Invoice />} />
          <Route path="invoice-check" ><Route path=":invoiceId" element={<InvoiceCheck />} /></Route>
          <Route path="search" element={<Search />} />
          <Route path="*" element={ <main style={{ padding: "1rem" }}> <p>There's nothing here!</p> </main>}/>
      </Route>

      <Route path="admin"> 
            <Route path="products" element={<ManageProducts />} />
            <Route path="brands" element={<ManageBrands />} />
            <Route path="invoices" element={<ManageInvoices />} />
      </Route>

    </Routes>
  </BrowserRouter>
);