import { Outlet } from "react-router-dom";
import './App.css';
import Axios from 'axios';
import Box from '@mui/material/Box';
import { pink } from '@mui/material/colors';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import HeaderBar from "./components/WebsiteAppBar.js";
import Footer from "./components/WebsiteFooter.js";

export const APIRequest = Axios.create({
  baseURL: 'https://localhost:7198/',
});


APIRequest.interceptors.response.use(function (response) {
  // HANYA KELUARGA 200AN YANG MASUK KE RESPONSE
  return response;
}, function (error) {
  // KELUARGA 100an, 300an, 400an DAN 500an MASUK KE CATCH
  if (error.response.status === 401) {
      // KASIH NOTIFIKASI SESI USER SUDAH HABIS
      // KICK USER FROM APPLICATION (SESI HABIS, USER DILEMPAR BALIK KE HALAMAN LOGIN)
  }
  return Promise.reject(error);
});



const theme = createTheme({
  palette: {
      primary: {
          main: pink[400]
      }
  }
});

export default function App() {
  return (
    <div className="base">

      { /* Theme Kustom/pink */ }
      <ThemeProvider theme={theme}>

      { /* Appbar */ }
      <Box sx={{ flexGrow: 1 }}><HeaderBar /></Box>

      { /* Konten */ }
      <div className="contentWrapper">
      <Outlet />
      </div>

    { /*Footer */ }
    <Footer />

    </ThemeProvider>
    </div>
  );
}