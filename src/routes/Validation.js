import React, { useEffect, useState } from "react";
import { Button, TextField, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { Link, useParams } from "react-router-dom";
import { APIRequest } from '../components/APICalls';

import "../App.css"
import HeaderBar from "../components/WebsiteAppBar";
import Footer from "../components/WebsiteFooter";

export default function Validation() {
    const { verifToken } = useParams();

    const [msg, setMsg] = useState("");
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        APIRequest({
            method: 'post',
            url: 'api/Login/VerifyEmailToken/',
            data: { verifToken: verifToken},
        })
            .then((res) => {
                if (res.status === 200) {
                    setMsg("success");
                }
            })
            .catch((err) => {
                console.log('err verif', err.response.data)
                setMsg("failed");
                setIsError(true);
            })
    }, []);
    return (
        <div style={{
            height: "100vh", backgroundColor: "#FF7596", margin: "0", color: "#FF7596", 
            display: "flex", justifyContent: "center", alignItems: "center"
        }} >
            {/* <Box style={{ width: "100%", height: "100%" }} >
                <HeaderBar />
            </Box> */}
            <Box style={{
                width: "auto", height: "auto", backgroundColor: "white", padding: "50px 100px", borderRadius: "30px",
                display: "flex", justifyContent: "center", alignItems: "center"
            }} > 
            {msg ?
                (isError
                    ? <Typography variant="h1" fontWeight="bold"
                        style={{ fontFamily: "'Barlow Condensed', sans-serif" }} >
                        Something When Wrong
                    </Typography>

                    : <Typography variant="h3" fontWeight="bold"
                        style={{ fontFamily: "'Barlow Condensed', sans-serif", textAlign: "center" }} >
                        Congratulations <br /> your account is active!! <br />
                        <Button variant="contained" component={Link} to="/login"
                            style={{
                                width: "300px", height: "50px", backgroundColor: "#FF7596", color: "white", marginTop: "30px",
                                fontSize: "20px", fontWeight: "bold"
                            }}>
                            Login
                        </Button>

                    </Typography>
                ) :
                <Typography variant="h2" fontWeight="bold" style={{ fontFamily: "'Barlow Condensed', sans-serif" }}>
                    Loading
                </Typography>
            }
             </Box>
            {/*<Box style={{ width: "100%", height: "100px", backgroundColor: "#FF7596" }} >
                <Footer />
            </Box> */}
            {/* <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <Button component={Link} to="/" variant="contained" style={{ backgroundColor: "#FF7596" }} >
                        Lanjut ke Beranda
                    </Button>
                </div> */}
        </div >
    );
}
