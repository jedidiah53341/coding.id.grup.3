import React, { useEffect, useState } from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import { Box, Toolbar, Grid, Container, Paper, Button, TextField, ButtonGroup, Card, CardContent, CardMedia, Typography } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Link } from "react-router-dom";
import SearchIcon from '@mui/icons-material/Search';
import HeaderbarAdmin from '../components/HeaderbarAdmin';
import ManageBrandDialogAddItem from '../components/ManageBrandDialogAddItem';
import ManageBrandDialogEditItem from '../components/ManageBrandDialogEditItem';
import { APIRequest } from '../components/APICalls';

const theme = createTheme({
    palette: {
        primary: {
            main: "#FF7596",
        },
        secondary: {
            main: '#4F4F4F',
        },
        white: {
            main: '#ffffff',
        },
    },
});

function ManageBrands() {

    /* useStates dan metode-metode untuk keperluan GET daftar semua merk */
    const [refreshPage, setRefreshPage] = useState(false);
    const [searchQuery, setSearchQuery] = useState();
    const [listOfBrands, setListOfBrands] = useState([]);
    const getListOfBrands = async () => {
        await APIRequest({
            method: 'GET', url: 'api/Brand/list-all-brands', params: { search: searchQuery },
        }).then((res) => { if (res.status === 200) { setListOfBrands(res.data); } }).catch((err) => { })
    }
    useEffect(() => { getListOfBrands(); }, [searchQuery, refreshPage])
    /* useStates untuk keperluan GET daftar semua merk */

    /* useStates untuk membuka dialog untuk POST merk baru */
    const [openAdd, setOpenAdd] = useState(false)
    /* useStates untuk membuka dialog untuk POST merk baru */

    /* useStates untuk membuka dialog untuk POST edit merk */
    const [editItemData, setEditItemData] = useState();
    const [openEdit, setOpenEdit] = useState(false);
    /* useStates untuk membuka dialog untuk POST edit merk */

    /* useStates dan metode-metode untuk keperluan POST hapus merk */
    const [idToDelete, setIdToDelete] = useState();
    const deleteBrand = async () => {
        /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
        APIRequest.defaults.headers.common['Authorization'] = 'bearer ' + JSON.parse(localStorage.getItem('loggedInUserAuthToken'));
        /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
        await APIRequest({
            method: 'POST', url: 'api/Brand/delete-brand', params: { brandId: idToDelete },
        }).then((res) => { if (res.status === 200) { console.log(res.status); setRefreshPage((status) => !status); } }).catch((err) => { })
    }
    useEffect(() => { getListOfBrands(); }, [searchQuery])
    /* useStates dan metode-metode untuk keperluan POST hapus merk */

    return (
        <ThemeProvider theme={theme}>
            <Box sx={{ display: 'flex' }}>
                <CssBaseline />
                {/* Header bar */}
                <HeaderbarAdmin />

                {/* Body Content */}
                <Box component="main"
                    sx={{ backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900], flexGrow: 1, height: '100vh', overflow: 'auto', }}
                >
                    <Toolbar />

                    {/* DIALOG ADD*/}
                    <ManageBrandDialogAddItem open={openAdd} onClose={() => { setOpenAdd(false); setRefreshPage((status) => !status); }} />

                    {/* DIALOG EDIT */}
                    <ManageBrandDialogEditItem open={openEdit} editItemData={editItemData} onClose={() => { setOpenEdit(false); setRefreshPage((status) => !status); }} />

                    <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                                    {/* TITLE */}
                                    <Typography variant="h5" color="secondary"
                                        style={{ fontWeight: "bold" }}
                                    >
                                        Manage Brand
                                    </Typography>

                                    {/* BOX PENCARIAN DATA */}
                                    <div style={{ display: 'flex', padding: "20px 0" }}>
                                        <TextField value={searchQuery} onChange={(e) => setSearchQuery(e.target.value)} id="input-with-icon-textfield" label="Pencarian Berdasarkan Nama Merk"
                                            InputProps={{
                                                endAdornment: <SearchIcon color="primary" />,
                                            }}
                                            variant="outlined"
                                            style={{ display: 'flex', flexGrow: 1, marginRight: '10px' }}
                                        />
                                        <Button variant='contained' color='primary'
                                            onClick={() => { setOpenAdd(true) }} style={{ width: 'auto', color: "white" }
                                            }>Tambah Baru</Button>
                                    </div>

                                    {listOfBrands.map((invoice) => (
                                        <Card key={invoice.id} style={{ margin: '2% 0' }}>
                                            <Grid container spacing={3}  >
                                                <Grid item xs={12} md={2} >
                                                    <CardMedia component="img" style={{ objectFit: 'contain' }} height="100" image={`data:image/jpeg;base64,${invoice.image}`} alt="Image" />
                                                </Grid>
                                                <Grid item xs={12} md={10} >
                                                    <CardContent>
                                                        <Typography gutterBottom variant="h5" component="div" >
                                                            {invoice.brand_name}
                                                        </Typography>
                                                        <Typography variant="body2" color="text.secondary">
                                                            {invoice.description}
                                                        </Typography>

                                                        <Grid container spacing={1}>
                                                            <Grid item xs={12} md={4}>
                                                                <Button fullWidth variant="outlined" color="primary" component={Link} to={`/category/${invoice.id}`} >
                                                                    Daftar Produk
                                                                </Button>
                                                            </Grid>
                                                            <Grid item xs={12} md={4}>
                                                                <Button fullWidth variant="outlined" color="primary" onClick={(e) => { e.preventDefault(); setOpenEdit(true); setEditItemData(invoice); }} >
                                                                    Edit Merk
                                                                </Button>
                                                            </Grid>
                                                            <Grid item xs={12} md={4}>
                                                                <Button fullWidth variant="outlined" color="primary" onClick={async (e) => { await e.preventDefault(); await setIdToDelete(invoice.id); await deleteBrand(); }} >
                                                                    Hapus Merk
                                                                </Button>
                                                            </Grid>
                                                        </Grid>

                                                    </CardContent>
                                                </Grid>
                                            </Grid>
                                        </Card>
                                    ))}
                                </Paper>
                            </Grid>
                        </Grid>
                    </Container>
                </Box>
            </Box>
        </ThemeProvider>
    );
}

export default ManageBrands;

// function Copyright(props) {
    // return (
    //     <Typography variant="body2" color="text.secondary" align="center" {...props}>
    //     {'Copyright © '}
    //     <Link color="inherit" href="/">
    //         1ELECTRONIC
    //     </Link>{' '}
    //     {new Date().getFullYear()}
    //     {'.'}
    //     </Typography>
    // );
    // }
