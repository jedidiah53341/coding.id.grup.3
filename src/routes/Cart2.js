import {useParams} from "react-router-dom";
import { useEffect, useState } from "react";
import DeleteIcon from '@mui/icons-material/Delete';
import { getNewArrivals } from "../jsonData/Data.js";
import { 
  Box, 
  Button, 
  Checkbox, 
  Grid,
  IconButton,
  Typography
} from '@mui/material';

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { Link } from "react-router-dom";
import CardMedia from '@mui/material/CardMedia';
import Rating from '@mui/material/Rating';
import Badge from '@mui/material/Badge';
import SellIcon from '@mui/icons-material/Sell';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import numberFormat from "../components/NumberFormat.js";
import { APIRequest } from "../components/APICalls.js";
import { Container } from "@mui/system";

export default function Cart2() {
  const [Open, setOpen] = useState(false)
  let params = useParams();
  const [quantity, setQuantity] = useState(1);
  const [subTotal, setSubTotal] = useState(0);

  /* useStates dan metode-metode untuk keperluan GET detail dari sebuah produk */
  const [detailOfAProduct, setDetailOfAProduct] = useState([]);
  const getdetailOfAProduct = async () => { await APIRequest({  method: 'GET', url: 'api/Product/product-detail', params: { productId: params.productId },
      }).then((res) => { if (res.status === 200) {  setDetailOfAProduct(res.data[0]); } }).catch((err) => { })}
  useEffect(() => { getdetailOfAProduct(); }, [params])
  /* useStates untuk keperluan GET detail dari sebuah produk */

   /* useStates dan metode-metode untuk keperluan GET daftar seluruh produk */
   const [listOfProducts, setlistOfProducts] = useState([]);
   const getlistOfProducts = async () => { await APIRequest({  method: 'GET', url: 'api/Product/list-all-products', params: { productId: "" },
       }).then((res) => { if (res.status === 200) {  setlistOfProducts(res.data); } }).catch((err) => { })}
   useEffect(() => { getlistOfProducts(); }, [params])
   /* useStates dan metode-metode untuk keperluan GET daftar seluruh produk */


  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 4,
      partialVisibilityGutter: 30
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 4,
      partialVisibilityGutter: 30
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
      partialVisibilityGutter: 30
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      partialVisibilityGutter: 30
    }
  };
   /* useStates dan method untuk keperluan menampilkan daftar produk dari cart/invoice tertentu */
  const [refreshPage, setRefreshPage] = useState(false);
  const [invoiceId, setInvoiceId] = useState(JSON.parse(localStorage.getItem('pendingCartId')));
  const [listOfProduct, setListOfProduct] = useState([]);
  const getListOfProductsByInvoiceId = async () => { await APIRequest({  method: 'GET', url: 'api/Product/list-products-by-invoice', params: { invoiceId: invoiceId },
  }).then((res) => { if (res.status === 200) {  setListOfProduct(res.data); } }).catch((err) => { })}
  useEffect(() => { getListOfProductsByInvoiceId();  }, [invoiceId])
  /* useStates dan method untuk keperluan menampilkan daftar produk dari cart/invoice tertentu */

  /* useStates dan metode-metode untuk keperluan POST hapus merk */
  const [idToDelete, setIdToDelete] = useState();
  const deleteItemFromCart = async () => { 
  await APIRequest({  method: 'POST', url: 'api/OrderInvoiceItem/delete-order-invoice-item', params: { orderInvoiceItemId: idToDelete },
  }).then((res) => { if (res.status === 200) {  console.log(res.status);setRefreshPage((status) => !status ); } }).catch((err) => { })}
  useEffect(() => { getListOfProductsByInvoiceId(); }, [refreshPage])
  /* useStates dan metode-metode untuk keperluan POST hapus merk */
    return (
      <div>
                <Grid >
              <Box sx={{flexGrow:1, margin:'10px 0'}} display='flex'>
                <Box><Checkbox style={{margin:'10px 10px'}} ></Checkbox>
                Select All</Box>
                <Box sx={{ margin:'auto 30px auto auto',alignItems:'flex-end', alignContent:'flex-end'}}>
                  <Button>Delete All</Button>
                </Box>
                
              </Box>
              <div style={{ height: '0px', border: '1px solid grey' }}></div>
          </Grid>
        {listOfProduct.map((item) => (
        <Grid >
          <Grid >
              <Box sx={{ margin:'10px 10px' }} display='flex'>
                <Box display='inline-flex'>
                <Checkbox style={{margin:'auto 10px'}} ></Checkbox>
                  <img src={`data:image/jpeg;base64,${item.image}`} width ='200'>
                  </img>
                </Box>
                <Box >
                  <Box>
                  <Typography sx={{margin:'40px auto auto 32px', alignItems:'flex-end', alignContent:'flex-end'}}>{item.name}</Typography>
                  </Box>
                  <Box sx={{margin:'100px auto auto 32px',  alignItems:'flex-end', alignContent:'flex-end'}}>
                  <Typography sx={{margin:'auto auto 0px 0px', alignItems:'flex-end', alignContent:'flex-end'}}>{numberFormat(item.total_price) }</Typography>
                  </Box>
                
                
                </Box>
                <Box sx={{margin:'auto 0 auto auto',right:'0px', alignItems:'flex-end', alignContent:'flex-end'}}>
                <span sx={{margin:'10px 10px', alignItems:'flex-end', alignContent:'flex-end'}}>
                            <Button sx={{right:'0', margin:'10px 10px', alignItems:'flex-end', alignContent:'flex-end'}} onClick={() => setQuantity((quantity=> quantity - 1))} >-</Button></span>{quantity}<span>
                            <Button sx={{right:'0', margin:'10px 10px', alignItems:'flex-end', alignContent:'flex-end'}}  onClick={() => setQuantity((quantity=> quantity + 1))} >+</Button></span>

                          <div>
                          <IconButton sx={{left:'100px', margin:'10px 10px', alignItems:'flex-end', alignContent:'flex-end'}}><DeleteIcon /></IconButton>
                            </div>
                </Box>
                  
              </Box>
              <div style={{ height: '0px', border: '1px solid grey' }}></div>
          </Grid>
        </Grid>
        ))}
        <Button variant='contained' style={{margin: '30px 0px'}} fullWidth onClick={(e)=>{
            e.preventDefault()
            setOpen(true)
          }}>Check Out</Button>
        <div style={{ padding: "50px 0" }}>
          <Box style={{
              width: '95%',
              height: 'auto',
              backgroundColor: "#FF7596",
              borderRadius: "20px",
              padding: '2%',
          }}>
              <Typography variant="h3" color="white"
                  style={{ fontSize: '25px', fontWeight: 'bold', padding: '0px 0 20px 0' }}>
                  Mungkin anda suka 
              </Typography>

                  <Carousel 
            swipeable={true}   

            draggable={true}
            responsive={responsive}
            autoPlay={true}
            >
                {listOfProducts.map((invoice) => (
                    <Card style={{ height:'100%', borderRadius: '5%', marginRight:'2%'  }} variant="outlined" >
                    <CardMedia style={{ objectFit: 'contain'}} component="img" height="160" image={`data:image/jpeg;base64,${invoice.image}`} alt={invoice.image} />
                        <CardContent>
                        <Rating name="read-only" value={invoice.rating} readOnly />

                        <Badge badgeContent={invoice.sold} color="primary">
                        <SellIcon color="action" />
                        </Badge>


                            <Typography variant="h6" component="div">
                            {invoice.name}
                            </Typography>
                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                            {numberFormat(invoice.price) }
                            </Typography>
                            <Typography variant="body2">
                            {invoice.description}
                            <br />
                            </Typography>

                        </CardContent>
                        <CardActions className="newArrivalsCardActions">
                            <Button component={Link} to={`/detail/${invoice.id}`}className="newArrivalsCardAction" variant="contained" size="small">Detail</Button>

                        </CardActions>
                    </Card>
                ))}
            </Carousel>

          </Box>
      </div>
      </div>
    );
  }


