// import * as React from 'react';
// import PropTypes from 'prop-types';
// import { alpha } from '@mui/material/styles';
// import Box from '@mui/material/Box';
// import Table from '@mui/material/Table';
// import TableBody from '@mui/material/TableBody';
// import TableCell from '@mui/material/TableCell';
// import TableContainer from '@mui/material/TableContainer';
// import TableHead from '@mui/material/TableHead';
// import TablePagination from '@mui/material/TablePagination';
// import TableRow from '@mui/material/TableRow';
// import TableSortLabel from '@mui/material/TableSortLabel';
// import Toolbar from '@mui/material/Toolbar';
// import Typography from '@mui/material/Typography';
// import Paper from '@mui/material/Paper';
// import Checkbox from '@mui/material/Checkbox';
// import IconButton from '@mui/material/IconButton';
// import Tooltip from '@mui/material/Tooltip';
// import FormControlLabel from '@mui/material/FormControlLabel';
// import Switch from '@mui/material/Switch';
// import DeleteIcon from '@mui/icons-material/Delete';
// import FilterListIcon from '@mui/icons-material/FilterList';
// import { visuallyHidden } from '@mui/utils';
// import { Alert, Button, Grid, Input } from '@mui/material';
// import { useEffect, useState } from "react";
// import Tambah from './tambah';
// import { Label } from '@mui/icons-material';
// import { APIRequest } from "../components/APICalls.js";
// import ReactDOMServer from 'react-dom/server';



// function createData(name,Detail,nilai) {
//   return {
//     name,
//     Detail,
//     nilai,
//   };
// }
// function Tambahn(){
//   const[quantity,setQuantity]= useState(1);
//   return(
//   <Grid>
//   <Typography variant='body1' fontWeight="bold">
//       <span style={{paddingLeft: '30px'}}>
//           <Button onClick={() => setQuantity((quantity=> quantity - 1))} >-</Button></span>{quantity}<span>
//           <Button  onClick={() => setQuantity((quantity=> quantity + 1))} >+</Button></span>
//   </Typography>
//   <div >
//       <IconButton><DeleteIcon /></IconButton>
//   </div>
//   <br/>
//   </Grid>
//   )
// }



// function descendingComparator(a, b, orderBy) {
//   if (b[orderBy] < a[orderBy]) {
//     return -1;
//   }
//   if (b[orderBy] > a[orderBy]) {
//     return 1;
//   }
//   return 0;
// }

// function getComparator(order, orderBy) {
//   return order === 'desc'
//     ? (a, b) => descendingComparator(a, b, orderBy)
//     : (a, b) => -descendingComparator(a, b, orderBy);
// }

// // This method is created for cross-browser compatibility, if you don't
// // need to support IE11, you can use Array.prototype.sort() directly

// function stableSort(array, comparator) {
//   const stabilizedThis = array.map((el, index) => [el, index]);
//   stabilizedThis.sort((a, b) => {
//     const order = comparator(a[0], b[0]);
//     if (order !== 0) {
//       return order;
//     }
//     return a[1] - b[1];
//   });
//   return stabilizedThis.map((el) => el[0]);
// }

// const headCells = [
//   {
//     id: 'name',
//     numeric: false,
//     disablePadding: true,
//     label: 'Select All',
//   },
//  {
//   id: 'Detail',
//      numeric: false,
//     disablePadding: false,
//     label: 'detail',
// },
//     {
//     id: 'nilai',
//     numeric: true,
//     disablePadding: false,
//     label: 'nilai',
//     },

// ];

// function EnhancedTableHead(props) {
//   const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } =
//     props;
//   const createSortHandler = (property) => (event) => {
//     onRequestSort(event, property);
//   };

//   return (
//     <TableHead>
//       <TableRow>
//         <TableCell padding="checkbox">
//           <Checkbox
//             color="primary"
//             indeterminate={numSelected > 0 && numSelected < rowCount}
//             checked={rowCount > 0 && numSelected === rowCount}
//             onChange={onSelectAllClick}
//             inputProps={{
//               'aria-label': 'select all',
//             }}
//           />
//         </TableCell>
//         {headCells.map((headCell) => (
//           <TableCell>
//           {headCell.label}
//           </TableCell>
//         )
//         )
//         }
//         {/* <Input type="button" value="Click Me" style="float: right;">dododo</Input> */}
//       </TableRow>
      
//     </TableHead>
//   );
// }

// EnhancedTableHead.propTypes = {
//   numSelected: PropTypes.number.isRequired,
//   onRequestSort: PropTypes.func.isRequired,
//   onSelectAllClick: PropTypes.func.isRequired,
//   order: PropTypes.oneOf(['asc', 'desc']).isRequired,
//   orderBy: PropTypes.string.isRequired,
//   rowCount: PropTypes.number.isRequired,
// };

// const EnhancedTableToolbar = (props) => {
//   const { numSelected } = props;
// };

// EnhancedTableToolbar.propTypes = {
//   numSelected: PropTypes.number.isRequired,
// };

// export default function Cart() {
//   const [order, setOrder] = React.useState('asc');
//   const [orderBy, setOrderBy] = React.useState('calories');
//   const [selected, setSelected] = React.useState([]);
//   const [page, setPage] = React.useState(0);
//   const [dense, setDense] = React.useState(false);
//   const [rowsPerPage, setRowsPerPage] = React.useState(5);
//   const [quantity, setQuantity] = useState(1);

//   /* useStates dan method untuk keperluan menampilkan daftar produk dari cart/invoice tertentu */
//   const [refreshPage, setRefreshPage] = useState(false);
//   const [invoiceId, setInvoiceId] = useState(JSON.parse(localStorage.getItem('pendingCartId')));
//   const [listOfProducts, setListOfProducts] = useState([]);
//   const getListOfProductsByInvoiceId = async () => { await APIRequest({  method: 'GET', url: 'api/Product/list-products-by-invoice', params: { invoiceId: invoiceId },
//   }).then((res) => { if (res.status === 200) {  setListOfProducts(res.data); } }).catch((err) => { })}
//   useEffect(() => { getListOfProductsByInvoiceId();  }, [invoiceId])
//   /* useStates dan method untuk keperluan menampilkan daftar produk dari cart/invoice tertentu */

//   /* useStates dan metode-metode untuk keperluan POST hapus merk */
//   const [idToDelete, setIdToDelete] = useState();
//   const deleteItemFromCart = async () => { 
//   await APIRequest({  method: 'POST', url: 'api/OrderInvoiceItem/delete-order-invoice-item', params: { orderInvoiceItemId: idToDelete },
//   }).then((res) => { if (res.status === 200) {  console.log(res.status);setRefreshPage((status) => !status ); } }).catch((err) => { })}
//   useEffect(() => { getListOfProductsByInvoiceId(); }, [refreshPage])
//   /* useStates dan metode-metode untuk keperluan POST hapus merk */
  

//   const rows = [
//     //   createData('Gingerbread', 356, 16.0, 49, 3.9),
//     createData(<img src={process.env.PUBLIC_URL + '/kulkas2Pintu.jpg'} width='200'/>,<div>ini detail produk<div>1203000</div></div>,Tambahn()),
//     createData(<img src={process.env.PUBLIC_URL + ' /tV30Inch.jpg'} width='200'/>,'ini detail produk',Tambahn()),
//     createData(<img src={`data:image/jpeg;base64,${listOfProducts.map.image}`}
//     width= "200" alt={listOfProducts.image} />,listOfProducts.name,Tambahn())
//     ];

//   const handleRequestSort = (event, property) => {
//     const isAsc = orderBy === property && order === 'asc';
//     setOrder(isAsc ? 'desc' : 'asc');
//     setOrderBy(property);
//   };

//   const handleSelectAllClick = (event) => {
//     if (event.target.checked) {
//       const newSelecteds = rows.map((n) => n.name);
//       setSelected(newSelecteds);
//       return;
//     }
//     setSelected([]);
//   };

//   const handleClick = (event, name) => {
//     const selectedIndex = selected.map((value)=>ReactDOMServer.renderToStaticMarkup(value)).indexOf(ReactDOMServer.renderToStaticMarkup(name));
//     let newSelected = [];

//     if (selectedIndex === -1) {
//       newSelected = newSelected.concat(selected, name);
//     } else if (selectedIndex === 0) {
//       newSelected = newSelected.concat(selected.slice(1));
//     } else if (selectedIndex === selected.length - 1) {
//       newSelected = newSelected.concat(selected.slice(0, -1));
//     } else if (selectedIndex > 0) {
//       newSelected = newSelected.concat(
//         selected.slice(0, selectedIndex),
//         selected.slice(selectedIndex + 1),
//       );
//     }
//     console.log(name)
//     console.log(newSelected)
//     console.log(selected)
//     console.log(<div></div>==<div></div>)
//     setSelected(newSelected);
//   };

//   const handleChangePage = (event, newPage) => {
//     setPage(newPage);
//   };

//   const handleChangeRowsPerPage = (event) => {
//     setRowsPerPage(parseInt(event.target.value, 10));
//     setPage(0);
//   };

//   const handleChangeDense = (event) => {
//     setDense(event.target.checked);
//   };

//   const isSelected = (name) => selected.map((value)=>ReactDOMServer.renderToStaticMarkup(value)).indexOf(ReactDOMServer.renderToStaticMarkup(name)) !== -1;

//   const Alert = (props={
//     isOpen: false,
//     onClose:()=>{},
//   })=> {
//     return <div style={{
//       bottom:'20px',
//       width: '100%',
//       backgroundColor: 'red',
//     }}></div>
//   };
//   const[open,setopen]=useState(false)

//   // Avoid a layout jump when reaching the last page with empty rows.
//   const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;
//   return (
//     <Box sx={{ width: '100%' }}>
//       <Paper sx={{ width: '100%', mb: 2 }}>
//         <EnhancedTableToolbar numSelected={selected.length} />
//         <Button variant='text'
//               color='inherit' 
//               style={{left:'1150px', margin:'10px 10px', alignItems:'flex-end', alignContent:'flex-end'}}
//               >Delete All</Button>
//         <TableContainer>
//           <Table
//             sx={{ minWidth: 750 }}
//             aria-labelledby="tableTitle"
//             size={dense ? 'small' : 'medium'}
//           >
//             <EnhancedTableHead
//               numSelected={selected.length}
//               order={order}
//               orderBy={orderBy}
//               onSelectAllClick={handleSelectAllClick}
//               onRequestSort={handleRequestSort}
//               rowCount={rows.length}
//             >apaa</EnhancedTableHead>
//             <TableBody>
//               {/* if you don't need to support IE11, you can replace the `stableSort` call with:
//                  rows.slice().sort(getComparator(order, orderBy)) */}
              
//               {stableSort(rows, getComparator(order, orderBy))
//                 .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
//                 .map((row, index) => {
//                   const isItemSelected = isSelected(row.name);
//                   const labelId = `enhanced-table-checkbox-${index}`;
//                   return (
//                     <TableRow
//                       hover
//                       onClick={(event) => handleClick(event, row.name)}
//                       role="checkbox"
//                       aria-checked={isItemSelected}
//                       tabIndex={-1}
//                       key={index}
//                       selected={isItemSelected}

//                     >
//                       <TableCell padding="checkbox">
//                         <Checkbox
//                           color="primary"
//                           checked={isItemSelected}
//                           inputProps={{
//                             'aria-labelledby': labelId,
//                           }}
//                         />
                        
//                       </TableCell>
//                       <TableCell
//                         component="th"
//                         id={labelId}
//                         scope="row"
//                         padding="none"
//                       >
//                         {row.name}
//                       </TableCell>
//                       <TableCell align="left"></TableCell>
//                       <TableCell align="center">{row.Detail}</TableCell>
//                       <TableCell align="right">{row.nilai}</TableCell>
//                       <TableCell align="right"></TableCell>

//                     </TableRow>
//                   );
//                 })}
//               {emptyRows > 0 && (
//                 <TableRow
//                   style={{
//                     height: (dense ? 33 : 53) * emptyRows,
//                   }}
//                 >
//                   <TableCell colSpan={6} />
//                 </TableRow>
//               )}
//             </TableBody>
//           </Table>
//         </TableContainer>
//       </Paper>
//       <Button variant='contained' fullWidth onClick={(e)=>{
//         e.preventDefault()
//         setopen(true)
//       }}>Check Out</Button>
//     </Box>
//   );
// }

// // import React from "react";
// // import { Button, CardMedia, che, Checkbox, FormControlLabel, Grid, styled } from "@mui/material";
// // import { useEffect, useState } from "react";
// // import { Link } from "react-router-dom";
// // import { APIRequest } from "../components/APICalls";
// // import numberFormat from "../components/NumberFormat";
// // import { DataGrid} from '@mui/x-data-grid';
// // import { Box, positions } from "@mui/system";
// // import { Style } from "@mui/icons-material";
// // //import kulkas2pintu from '../public/assets\image' ;




// //       const btnstyle={margin: '10px 15px'}
// //       const Image = styled.img``
// //   return (
// //     <div >
// //       <FormControlLabel control={<Checkbox />} label="Select All" style={btnstyle} pading='20px'/>
// //     </div>
// //   );
// // }

// import * as React from 'react';
// import PropTypes from 'prop-types';
// import { alpha } from '@mui/material/styles';
// import Box from '@mui/material/Box';
// import Table from '@mui/material/Table';
// import TableBody from '@mui/material/TableBody';
// import TableCell from '@mui/material/TableCell';
// import TableContainer from '@mui/material/TableContainer';
// import TableHead from '@mui/material/TableHead';
// import TablePagination from '@mui/material/TablePagination';
// import TableRow from '@mui/material/TableRow';
// import TableSortLabel from '@mui/material/TableSortLabel';
// import Toolbar from '@mui/material/Toolbar';
// import Typography from '@mui/material/Typography';
// import Paper from '@mui/material/Paper';
// import Checkbox from '@mui/material/Checkbox';
// import IconButton from '@mui/material/IconButton';
// import Tooltip from '@mui/material/Tooltip';
// import FormControlLabel from '@mui/material/FormControlLabel';
// import Switch from '@mui/material/Switch';
// import DeleteIcon from '@mui/icons-material/Delete';
// import FilterListIcon from '@mui/icons-material/FilterList';
// import { visuallyHidden } from '@mui/utils';
// import { Alert, Button, Grid, Input } from '@mui/material';
// import { useEffect, useState } from "react";
// import Tambah from './tambah';
// import { Label } from '@mui/icons-material';
// import { APIRequest } from "../components/APICalls.js";
// import ReactDOMServer from 'react-dom/server';



// function createData(name,Detail,nilai) {
//   return {
//     name,
//     Detail,
//     nilai,
//   };
// }
// // function Tambahn(){
// //   
// //   return(
//   <Grid>
//   <Typography variant='body1' fontWeight="bold">
//       <span style={{paddingLeft: '30px'}}>
//           <Button onClick={() => setQuantity((quantity=> quantity - 1))} >-</Button></span>{quantity}<span>
//           <Button  onClick={() => setQuantity((quantity=> quantity + 1))} >+</Button></span>
//   </Typography>
//   <div >
//       <IconButton><DeleteIcon /></IconButton>
//   </div>
//   <br/>
//   </Grid>
// //   )
// // }



// function descendingComparator(a, b, orderBy) {
//   if (b[orderBy] < a[orderBy]) {
//     return -1;
//   }
//   if (b[orderBy] > a[orderBy]) {
//     return 1;
//   }
//   return 0;
// }

// function getComparator(order, orderBy) {
//   return order === 'desc'
//     ? (a, b) => descendingComparator(a, b, orderBy)
//     : (a, b) => -descendingComparator(a, b, orderBy);
// }

// // This method is created for cross-browser compatibility, if you don't
// // need to support IE11, you can use Array.prototype.sort() directly

// function stableSort(array, comparator) {
//   const stabilizedThis = array.map((el, index) => [el, index]);
//   stabilizedThis.sort((a, b) => {
//     const order = comparator(a[0], b[0]);
//     if (order !== 0) {
//       return order;
//     }
//     return a[1] - b[1];
//   });
//   return stabilizedThis.map((el) => el[0]);
// }

// const headCells = [
//   {
//     id: 'name',
//     numeric: false,
//     disablePadding: true,
//     label: 'Select All',
//   },
//  {
//   id: 'Detail',
//      numeric: false,
//     disablePadding: false,
//     label: 'detail',
// },
//     {
//     id: 'nilai',
//     numeric: true,
//     disablePadding: false,
//     label: 'nilai',
//     },

// ];

// function EnhancedTableHead(props) {
//   const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } =
//     props;
//   const createSortHandler = (property) => (event) => {
//     onRequestSort(event, property);
//   };

//   return (
//     <TableHead>
//       <TableRow>
//         <TableCell padding="checkbox">
//           <Checkbox
//             color="primary"
//             indeterminate={numSelected > 0 && numSelected < rowCount}
//             checked={rowCount > 0 && numSelected === rowCount}
//             onChange={onSelectAllClick}
//             inputProps={{
//               'aria-label': 'select all',
//             }}
//           />
//         </TableCell>
//         {headCells.map((headCell) => (
//           <TableCell>
//           {headCell.label}
//           </TableCell>
//         )
//         )
//         }
//         {/* <Input type="button" value="Click Me" style="float: right;">dododo</Input> */}
//       </TableRow>
      
//     </TableHead>
//   );
// }

// EnhancedTableHead.propTypes = {
//   numSelected: PropTypes.number.isRequired,
//   onRequestSort: PropTypes.func.isRequired,
//   onSelectAllClick: PropTypes.func.isRequired,
//   order: PropTypes.oneOf(['asc', 'desc']).isRequired,
//   orderBy: PropTypes.string.isRequired,
//   rowCount: PropTypes.number.isRequired,
// };

// const EnhancedTableToolbar = (props) => {
//   const { numSelected } = props;
// };

// EnhancedTableToolbar.propTypes = {
//   numSelected: PropTypes.number.isRequired,
// };

// export default function Cart() {
//   const [order, setOrder] = React.useState('asc');
//   const [orderBy, setOrderBy] = React.useState('calories');
//   const [selected, setSelected] = React.useState([]);
//   const [page, setPage] = React.useState(0);
//   const [dense, setDense] = React.useState(false);
//   const [rowsPerPage, setRowsPerPage] = React.useState(5);
//   const [quantity, setQuantity] = useState(1);

//   /* useStates dan method untuk keperluan menampilkan daftar produk dari cart/invoice tertentu */
//   const [refreshPage, setRefreshPage] = useState(false);
//   const [invoiceId, setInvoiceId] = useState(JSON.parse(localStorage.getItem('pendingCartId')));
//   const [listOfProducts, setListOfProducts] = useState([]);
//   const getListOfProductsByInvoiceId = async () => { await APIRequest({  method: 'GET', url: 'api/Product/list-products-by-invoice', params: { invoiceId: invoiceId },
//   }).then((res) => { if (res.status === 200) {  setListOfProducts(res.data); } }).catch((err) => { })}
//   useEffect(() => { getListOfProductsByInvoiceId();  }, [invoiceId])
//   /* useStates dan method untuk keperluan menampilkan daftar produk dari cart/invoice tertentu */

//   /* useStates dan metode-metode untuk keperluan POST hapus merk */
//   const [idToDelete, setIdToDelete] = useState();
//   const deleteItemFromCart = async () => { 
//   await APIRequest({  method: 'POST', url: 'api/OrderInvoiceItem/delete-order-invoice-item', params: { orderInvoiceItemId: idToDelete },
//   }).then((res) => { if (res.status === 200) {  console.log(res.status);setRefreshPage((status) => !status ); } }).catch((err) => { })}
//   useEffect(() => { getListOfProductsByInvoiceId(); }, [refreshPage])
//   /* useStates dan metode-metode untuk keperluan POST hapus merk */
  

//   const rows = [
//     //   createData('Gingerbread', 356, 16.0, 49, 3.9),
//     createData(<img src={process.env.PUBLIC_URL + '/kulkas2Pintu.jpg'} width='200'/>,<div>ini detail produk<div>1203000</div></div>,'Tambahn()'),
//     createData(<img src={process.env.PUBLIC_URL + ' /tV30Inch.jpg'} width='200'/>,'ini detail produk','Tambahn()'),
//     createData(<img src={`data:image/jpeg;base64,${listOfProducts.p.image}`}
//     width= "200" alt={listOfProducts.image} />,listOfProducts.name,'Tambahn()')
//     ];

//   const handleRequestSort = (event, property) => {
//     const isAsc = orderBy === property && order === 'asc';
//     setOrder(isAsc ? 'desc' : 'asc');
//     setOrderBy(property);
//   };

//   const handleSelectAllClick = (event) => {
//     if (event.target.checked) {
//       const newSelecteds = rows.map((n) => n.name);
//       setSelected(newSelecteds);
//       return;
//     }
//     setSelected([]);
//   };

//   const handleClick = (event, name) => {
//     const selectedIndex = selected.map((value)=>ReactDOMServer.renderToStaticMarkup(value)).indexOf(ReactDOMServer.renderToStaticMarkup(name));
//     let newSelected = [];

//     if (selectedIndex === -1) {
//       newSelected = newSelected.concat(selected, name);
//     } else if (selectedIndex === 0) {
//       newSelected = newSelected.concat(selected.slice(1));
//     } else if (selectedIndex === selected.length - 1) {
//       newSelected = newSelected.concat(selected.slice(0, -1));
//     } else if (selectedIndex > 0) {
//       newSelected = newSelected.concat(
//         selected.slice(0, selectedIndex),
//         selected.slice(selectedIndex + 1),
//       );
//     }
//     console.log(name)
//     console.log(newSelected)
//     console.log(selected)
//     console.log(<div></div>==<div></div>)
//     setSelected(newSelected);
//   };

//   const handleChangePage = (event, newPage) => {
//     setPage(newPage);
//   };

//   const handleChangeRowsPerPage = (event) => {
//     setRowsPerPage(parseInt(event.target.value, 10));
//     setPage(0);
//   };

//   const handleChangeDense = (event) => {
//     setDense(event.target.checked);
//   };

//   const isSelected = (name) => selected.map((value)=>ReactDOMServer.renderToStaticMarkup(value)).indexOf(ReactDOMServer.renderToStaticMarkup(name)) !== -1;

//   const Alert = (props={
//     isOpen: false,
//     onClose:()=>{},
//   })=> {
//     return <div style={{
//       bottom:'20px',
//       width: '100%',
//       backgroundColor: 'red',
//     }}></div>
//   };
//   const[open,setopen]=useState(false)

//   // Avoid a layout jump when reaching the last page with empty rows.
//   const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;
//   return (
//     <Box sx={{ width: '100%' }}>
//       <Paper sx={{ width: '100%', mb: 2 }}>
//         <EnhancedTableToolbar numSelected={selected.length} />
//         <Button variant='text'
//               color='inherit' 
//               style={{left:'1150px', margin:'10px 10px', alignItems:'flex-end', alignContent:'flex-end'}}
//               >Delete All</Button>
//         <TableContainer>
//           <Table
//             sx={{ minWidth: 750 }}
//             aria-labelledby="tableTitle"
//             size={dense ? 'small' : 'medium'}
//           >
//             <EnhancedTableHead
//               numSelected={selected.length}
//               order={order}
//               orderBy={orderBy}
//               onSelectAllClick={handleSelectAllClick}
//               onRequestSort={handleRequestSort}
//               rowCount={rows.length}
//             >apaa</EnhancedTableHead>
//             <TableBody>
//               {/* if you don't need to support IE11, you can replace the `stableSort` call with:
//                  rows.slice().sort(getComparator(order, orderBy)) */}
//               {stableSort(rows, getComparator(order, orderBy))
//                 .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
//                 .map((row, index) => {
//                   const isItemSelected = isSelected(row.name);
//                   const labelId = `enhanced-table-checkbox-${index}`;
//                   return (
//                     <TableRow
//                       hover
//                       onClick={(event) => handleClick(event, row.name)}
//                       role="checkbox"
//                       aria-checked={isItemSelected}
//                       tabIndex={-1}
//                       key={index}
//                       selected={isItemSelected}

//                     >
//                       <TableCell padding="checkbox">
//                         <Checkbox
//                           color="primary"
//                           checked={isItemSelected}
//                           inputProps={{
//                             'aria-labelledby': labelId,
//                           }}
//                         />
                        
//                       </TableCell>
//                       <TableCell
//                         component="th"
//                         id={labelId}
//                         scope="row"
//                         padding="none"
//                       >
//                         {row.name}
//                       </TableCell>
//                       <TableCell align="left"></TableCell>
//                       <TableCell align="center">{row.Detail}</TableCell>
//                       <TableCell align="right">{row.nilai}</TableCell>
//                       <TableCell align="right"></TableCell>

//                     </TableRow>
//                   );
//                 })}
//               {emptyRows > 0 && (
//                 <TableRow
//                   style={{
//                     height: (dense ? 33 : 53) * emptyRows,
//                   }}
//                 >
//                   <TableCell colSpan={6} />
//                 </TableRow>
//               )}
//             </TableBody>
//           </Table>
//         </TableContainer>
//       </Paper>
//       <Button variant='contained' fullWidth onClick={(e)=>{
//         e.preventDefault()
//         setopen(true)
//       }}>Check Out</Button>
//     </Box>
//   );
// }

// // import React from "react";
// // import { Button, CardMedia, che, Checkbox, FormControlLabel, Grid, styled } from "@mui/material";
// // import { useEffect, useState } from "react";
// // import { Link } from "react-router-dom";
// // import { APIRequest } from "../components/APICalls";
// // import numberFormat from "../components/NumberFormat";
// // import { DataGrid} from '@mui/x-data-grid';
// // import { Box, positions } from "@mui/system";
// // import { Style } from "@mui/icons-material";
// // //import kulkas2pintu from '../public/assets\image' ;




// //       const btnstyle={margin: '10px 15px'}
// //       const Image = styled.img``
// //   return (
// //     <div >
// //       <FormControlLabel control={<Checkbox />} label="Select All" style={btnstyle} pading='20px'/>
// //     </div>
// //   );
// // }