import { Alert, Avatar, Button, Checkbox, FormControlLabel, Grid, InputLabel, Link, MenuItem, Paper, Select, TextField, Typography, Box } from '@mui/material'
import React, { useEffect, useState, useRef } from 'react';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { pink } from '@mui/material/colors';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import CircularProgress from '@mui/material/CircularProgress';
import Fade from '@mui/material/Fade';
import { APIRequest } from '../components/APICalls';
import { useNavigate } from 'react-router-dom';
import '../App.css';

const theme = createTheme({
    palette: {
        primary: {
            main: "#FF7596",
        },
        secondary: {
            main: '#4F4F4F',
        },
    },
});

const Register = () => {

    let navigate = useNavigate();

    /* useStates untuk keperluan POST akun baru */
    const [username, setUsername] = useState("");
    const [firstName, setFirstName] = useState("Sample");
    const [lastName, setLastName] = useState("Account");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [role, setRole] = useState("buyer");
    /* useStates untuk keperluan POST akun baru */

    /* useStates cek kelayakan password */
    const [passwordConfirmation, setPasswordConfirmation] = useState("");

    const [policiesAgreement, setPoliciesAgreement] = useState(false);

    function containsSymbols(str) { const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/; return specialChars.test(str); }
    function containsNumber(str) { const specialChars = /[`0123456789]/; return specialChars.test(str); }
    const [allowedPassword, setAllowedPassword] = useState(false);
    useEffect(() => {
        if (password.length >= 6 && containsSymbols(password) && containsNumber(password) && password !== "" && (password === passwordConfirmation) && policiesAgreement) {
            setAllowedPassword(true);
        } else { setAllowedPassword(false) }
    }, [password, passwordConfirmation, policiesAgreement])
    /* useStates cek kelayakan password */

    /* Method to POST akun baru */
    const register = () => {
        const postDataa = { username: username, customer_name: firstName + " " + lastName, email: email, password: password, role: role, active: false };
        APIRequest({
            method: 'POST', url: 'api/Login/Register', data: postDataa,
        }).then((res) => {
            if (res.status === 200) {
                console.log(res.status)
                // setRegistered(true)
                navigate("/login");
            }
        }).catch((err) => { console.log(err.response.data) })
    }
    /* Method to POST akun baru */

    const [query, setQuery] = useState('idle');
    const timerRef = React.useRef();
    // const [registered, setRegistered] = useState(false)
    const handleClickQuery = () => {
        if (timerRef.current) {
            clearTimeout(timerRef.current);
        }

        if (query !== 'idle') {
            setQuery('idle');
            return;
        }

        setQuery('progress');
        timerRef.current = window.setTimeout(() => {
            setQuery('success');
        }, 5000);
    };

    useEffect(() => {
        clearTimeout(timerRef.current);
    }, []);
    const btstyle = { margin: '10px 0', color: 'white' }
    const btnstyle = { margin: '10px 0px 10px 10px' }
    return (
        <ThemeProvider theme={theme}>
            <div style={{ background: '#FF7596', padding: '2%', height: '100%', width: '96%', }}>
                <Grid >
                    <Paper className="registerForm" >
                        <Grid align='center'>
                            <Avatar sx={{ bgcolor: pink[500] }}><LockOutlinedIcon /></Avatar>
                            <h1>Sign Up</h1>
                            <Link style={{ color: '#FF7596', textDecoration: 'none' }} href="/">
                                <Typography variant='h6' style={{ marginTop: '-10px' }}>1Electronic</Typography>
                            </Link>
                        </Grid>
                        <form onSubmit={(e) => { e.preventDefault(); register(); handleClickQuery(); }} >
                            <TextField value={username}
                                onChange={(e) => setUsername(e.target.value)}
                                style={btnstyle} label='Username' placeholder='Enter Username' fullWidth required />
                            <TextField
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                style={btnstyle} label='Email' placeholder='Enter Email' fullWidth required />

                            <Grid container spacing={3}  >
                            <Grid item xs={12} md={6} >
                                <TextField
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                style={btnstyle} label='Password' placeholder='Enter Password' type='password' fullWidth required />
                            </Grid>
                            <Grid item xs={12} md={6} >
                                <TextField
                                value={passwordConfirmation}
                                onChange={(e) => setPasswordConfirmation(e.target.value)}
                                style={btnstyle} label='Password Confirmation' placeholder='Enter Password Confirmation' type='password' fullWidth required />
                            </Grid>
                            </Grid>

                            <FormControl>
                            <FormLabel id="demo-row-radio-buttons-group-label">Tipe Akun</FormLabel>
                            <RadioGroup
                                row
                                aria-labelledby="demo-row-radio-buttons-group-label"
                                value={role}
                                style={btnstyle}
                                onChange={(e) => setRole(e.target.value)}
                            >
                                <FormControlLabel value="buyer" control={<Radio />} label="Buyer" />
                                <FormControlLabel value="admin" control={<Radio />} label="Admin" />
                            </RadioGroup>
                            </FormControl>

                            <br/>

                            <FormControlLabel
                                control={
                                    <Checkbox
                                        name='checked'
                                        color='primary'
                                        checked={policiesAgreement}
                                        onClick={() => {setPoliciesAgreement((policy) => !policy)}}
                                    />
                                }
                                label='By registering you have agreed to all applicable policies and conditions'
                            />
                            <Box sx={{ textAlign: 'center' }}>
                                {query === 'success' ? (
                                    <Typography variant='h6'>Registered!</Typography>
                                ) : (
                                    <Fade
                                        in={query === 'progress'}
                                        style={{
                                            transitionDelay: query === 'progress' ? '800ms' : '0ms',
                                        }}
                                        unmountOnExit
                                    >
                                        <CircularProgress color='primary' size={50} />
                                    </Fade>
                                )}
                            </Box>
                            {
                                allowedPassword ?
                                    <Button /* onClick={} */ type='submit' variant='contained' color='primary' fullWidth style={btstyle} >
                                        Sign Up
                                    </Button>
                                    :
                                    <div>
                                        <Alert severity="warning">Demi keamanan akun anda, pastikan password anda :
                                            <ul>
                                                <li>Memiliki setidaknya 6 digit karakter.</li>
                                                <li>Memiliki setidaknya 1 simbol.</li>
                                                <li>Memiliki setidaknya 1 angka.</li>
                                            </ul></Alert>
                                        <Button disabled variant='contained' color='primary' fullWidth style={btstyle}>Sign Up</Button>
                                    </div>
                            }

                        </form>

                        <Typography >
                            <div>
                                Already have an acount?
                                <Link href="Login" fontWeight="bold">
                                    &nbsp; Sign In
                                </Link>
                            </div>
                        </Typography>
                    </Paper>
                </Grid>
            </div>
        </ThemeProvider>
    )
}

export default Register
