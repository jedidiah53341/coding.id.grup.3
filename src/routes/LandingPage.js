
import LandingPageCartWrapper from '../components/LandingPageCartWrapper.js';
import LandingPageCarousel from '../components/LandingPageCarousel.js';
import LandingPageCategoriesWrapper from '../components/LandingPageCategoriesWrapper.js';
import LandingPageNewArrivalsWrapper from '../components/LandingPageNewArrivalsWrapper.js';

export default function LandingPage() {
  
  return (
    <div className="landingPageWrapper">

    <div className="outerCartWrapper" >
        <LandingPageCartWrapper />
    </div>

    <div className="landingPageCarouselWrapper" >
        <LandingPageCarousel />
    </div>
    
    <div className="categoryWrapper" >
        <LandingPageCategoriesWrapper />
    </div>

    <div className="newArrivalsWrapper" >
        <LandingPageNewArrivalsWrapper />
    </div>

    </div>
  );
}