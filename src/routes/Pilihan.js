import { Avatar, Button, Checkbox, FormControlLabel, Grid, Link, Paper, TextField, Typography } from '@mui/material'
import React from 'react';
import { pink } from '@mui/material/colors';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';


const Pilihan = () => {
    const paperStyle = { padding: 20, height: '70vh', width: 290, margin: '0px auto' }
    const btstyle = { margin: '8px 0' }
    return (
        <div style={{ background: '#FF7596', padding: '2%', height: '100%', width: '100%', position: 'fixed' }}>
            <Grid >
                <Paper elevation={20} style={paperStyle}>
                    <Grid align='center'>
                        <Avatar sx={{ bgcolor: pink[500] }}><LockOutlinedIcon /></Avatar>
                        <h2>Sign In</h2>

                        <Link href="/" style={{ textDecoration: 'none', color: '#FF7596' }}>
                            <Typography
                                variant="body2"
                            > 1Electronic
                            </Typography>
                        </Link>


                    </Grid>
                    <Button type='submit' variant='contained' color='primary' fullWidth style={btstyle} href="/login">Pengguna</Button>
                    <Button type='submit' variant='contained' color='primary' fullWidth style={btstyle} href='/loginadmin'>Admin</Button>
                </Paper>
            </Grid>
        </div>
    )
}

export default Pilihan
