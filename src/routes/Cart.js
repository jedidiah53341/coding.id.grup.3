import {useParams} from "react-router-dom";
import { useEffect, useState } from "react";
import DeleteIcon from '@mui/icons-material/Delete';
import { getNewArrivals } from "../jsonData/Data.js";
import { 
  Alert,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Box, 
  Button, 
  Checkbox, 
  Grid,
  IconButton,
  Paper,
  Typography,
  Collapse
} from '@mui/material';

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { Link } from "react-router-dom";
import CardMedia from '@mui/material/CardMedia';
import Rating from '@mui/material/Rating';
import Badge from '@mui/material/Badge';
import SellIcon from '@mui/icons-material/Sell';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import numberFormat from "../components/NumberFormat.js";
import { APIRequest } from "../components/APICalls.js";
import { color, Container } from "@mui/system";
import PaperCart from "../components/PaperCart.js";
import { LayersOutlined } from "@mui/icons-material";

export default function Cart() {
  const [open, setOpen] = useState(false)
  const [openAll, setOpenAll] = useState(false)
  let params = useParams();
  const [quantity, setQuantity] = useState(1);
  const [subTotal, setSubTotal] = useState(0);
  const [openAlert, setOpenAlert] = useState(false);
  const[openRincian, setOpenRincian] = useState(false)

   /* useStates dan metode-metode untuk keperluan GET daftar seluruh produk */
   const [listOfProducts, setlistOfProducts] = useState([]);
   const getlistOfProducts = async () => { await APIRequest({  method: 'GET', url: 'api/Product/list-all-products', params: { productId: "" },
       }).then((res) => { if (res.status === 200) {  setlistOfProducts(res.data); } }).catch((err) => { })}
   useEffect(() => { getlistOfProducts(); }, [params])
   /* useStates dan metode-metode untuk keperluan GET daftar seluruh produk */

   /* useStates dan metode-metode untuk keperluan GET daftar seluruh produk */
   const [openDeleteAllConfirmationDialog, setOpenDeleteAllConfirmationDialog] = useState(false);
   /* useStates dan metode-metode untuk keperluan GET daftar seluruh produk */

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 4,
      partialVisibilityGutter: 30
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 4,
      partialVisibilityGutter: 30
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
      partialVisibilityGutter: 30
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      partialVisibilityGutter: 30
    }
  };
   /* useStates dan method untuk keperluan menampilkan daftar produk dari cart/invoice tertentu */
  const [refreshPage, setRefreshPage] = useState(false);
  const [invoiceId, setInvoiceId] = useState(JSON.parse(localStorage.getItem('pendingCartId')));
  const [listOfProduct, setListOfProduct] = useState([]);
  const getListOfProductsByInvoiceId = async () => { await APIRequest({  method: 'GET', url: 'api/Product/list-products-by-invoice', params: { invoiceId: invoiceId },
  }).then((res) => { if (res.status === 200) {  setListOfProduct(res.data); } }).catch((err) => { })}
  useEffect(() => { getListOfProductsByInvoiceId();  }, [invoiceId])
  /* useStates dan method untuk keperluan menampilkan daftar produk dari cart/invoice tertentu */

  /* useStates dan metode-metode untuk keperluan POST hapus merk */
  const [idToDelete, setIdToDelete] = useState();
  const deleteItemFromCart = async () => { 
  await APIRequest({  method: 'POST', url: 'api/OrderInvoiceItem/delete-order-invoice-item', params: { orderInvoiceItemId: idToDelete },
  }).then((res) => { if (res.status === 200) {  console.log(res.status);setRefreshPage((status) => !status ); } }).catch((err) => { })}
  useEffect(() => { getListOfProductsByInvoiceId(); }, [refreshPage])
  /* useStates dan metode-metode untuk keperluan POST hapus merk */

  const EditOrderInvoiceItem = () => {
    const postDataa = { quantity: quantity, total_price: subTotal};
    APIRequest({ method: 'POST',url: 'api/OrderInvoiceItem/edit-order-invoice-item',data: postDataa,params: { loggedInUserId: JSON.parse(localStorage.getItem('loggedInUserId')) },
    }).then((res) => {
        if (res.status === 200) {
            setOpenAlert(true);
            localStorage.setItem('pendingCartId', JSON.stringify(res.data.at(-1)));
            setTimeout(() => setOpenAlert(false), 2000);
        }
    }).catch((err) => {console.log(err.response.data)})
    }

    
  // let sum =0
  // for (let index = 0; index < listOfProduct.length; index++) {
  //   sum += listOfProduct[listOfProduct.length-1].quantity
  // }

var lodash = require('lodash');
const arr = listOfProduct.map((i) =>{
   return i.quantity
});
var sum = lodash.sum(arr)

const arrTotalHarga = listOfProduct.map((i) =>{
  return i.total_price
});
var sumTotalPrice = lodash.sum(arrTotalHarga)


  // console.log(sum)

  // let sum_total =0
  // for (let index = 0; index < listOfProduct.length; index++) {
  //   sum_total += listOfProduct.total_price
  // }
  // //console.log(sum_total)

  /* useStates dan metode-metode untuk keperluan POST perubahan selectbox / perubahan apakah barang disertakan ke checkout atau tidak */
  const handleItemInclusion = async (itemId,itemStatus) => {
  await APIRequest({  method: 'POST', url: 'api/OrderInvoiceItem/order-item-inclusion', params: { orderInvoiceItemId: itemId, orderInvoiceItemStatus: itemStatus },
  }).then((res) => { if (res.status === 200) {  setRefreshPage((status) => !status ); } }).catch((err) => { })
  }
  /* useStates dan metode-metode untuk keperluan POST perubahan selectbox / perubahan apakah barang disertakan ke checkout atau tidak */

  const handleQuantity = async (orderInvoiceItemId, orderInvoiceId, currentQuantity, currentTotalPrice, act) => {
  await APIRequest({  method: 'POST', url: 'api/OrderInvoiceItem/handle-quantity', params: { orderInvoiceItemId: orderInvoiceItemId, orderInvoiceId: orderInvoiceId, currentQuantity: currentQuantity, currentTotalPrice: currentTotalPrice, act: act },
  }).then((res) => { if (res.status === 200) {   setRefreshPage((status) => !status ); } }).catch((err) => { })
  }

  const [allToDelete, setallToDelete] = useState();
  const deleteAll = async () => { 
  await APIRequest({  method: 'POST', url: 'api/OrderInvoiceItem/delete-all-order-invoice-item', params: { orderInvoiceId: invoiceId },
  }).then((res) => { if (res.status === 200) {  console.log(res.data);setRefreshPage((status) => !status ); } }).catch((err) => { })}
  useEffect(() => { getListOfProductsByInvoiceId(); }, [refreshPage])
  /* useStates dan metode-metode untuk keperluan POST hapus merk */


  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  
  const handleClickOpenAll = () => {
    setOpenAll(true);
  };

  const handleCloseAll = () => {
    setOpenAll(false);
  };

const [checkedAll, setCheckedAll] = useState(false);
const [checked, setChecked] = useState({listOfProduct});
const toggleCheck = (inputName,index) => {
  setChecked((prevState) => {
    const newState = { ...prevState };
    newState[inputName] = !prevState[inputName];
    return newState;
  });
  // setlistOfProduct(()=>{
  //   const newState = [ ...prevState ];
  //   newState[index] = !prevState[index];
  //   return newState;
  // })
};
const selectAll = (value) => {
  setCheckedAll(value);
  // setChecked((prevState) => {
  //   const newState = { ...prevState };
  //   for (const inputName in newState) {
  //     newState[inputName] = value;
  //   }
  //   return newState;
  // });
  listOfProduct.forEach((Item)=>{
    if(value){
      handleItemInclusion(Item.id,'notIncluded')
      
    }else{
      handleItemInclusion(Item.id,'included')
    }
  })
};
useEffect(() => {
  let allChecked = true;
  for (const inputName in checked) {
    if (checked[inputName] === false) {
      allChecked = false;
    }
  }
  if (allChecked) {
    setCheckedAll(true);
  } else {
    setCheckedAll(false);
  }
}, [checked]);


//#################################################
//#################################################
  if(listOfProduct == 0){
    return(
      <div 
      //className="cartEmpty"
      >
        <Box sx={{width:'100%', 
        minHeight:'400px',
        textAlign: 'center'
        }}
        
        >
        <Typography sx={{alignItems:'center', alignContent:'center', margin: 'auto auto'}}
        left="50%" right='50%'>
            <h3>
            Ooops! kamu belum memasukan barang belanjaan
            </h3>
            <Button sx={{
              textTransform: 'capitalize', 
              margin:'auto 134px',
              alignItems:'center', 
              alignContent:'center'}} 
              variant="contained" href="/" padding='10px 10px'>Kembali ke Beranda</Button>
            </Typography>
            
        </Box>
          

          
          <Box
          display='flex'
      sx={{
        width: '100%',
        margin:'auto auto 0px auto',
        border: '1px solid grey',
        backgroundColor:'white'
      }}
      position='fixed'
      bottom={0}
      
    >
        <Typography style={{width:'85%'}}>

          <Typography sx={{margin:'auto auto 10px 50px'}}>
            <h3>
              Rincian Belanjaan Total
            </h3>
          </Typography>
          <Typography display='flex' sx={{margin:'auto auto 10px 50px'}}>
            Jumlah Produk
            <Typography sx={{margin:'auto 0px 0px auto'}}>{sum}</Typography>
          </Typography> 
          <div style={{ height: '0px', border: '1px solid grey', margin:'0px 0px 0px 50px' }}></div>
          <Typography display='flex' sx={{margin:'0px 0px 0px 50px'}}>
          <h4>
          Total
          </h4>
          <Typography sx={{margin:'0px 0px auto auto'}}><h4>{numberFormat(sumTotalPrice)}</h4></Typography>
        </Typography>
        
        </Typography>
        <Button variant="contained"
         disabled sx={{textTransform: 'capitalize',margin:'auto auto auto 20px'}}  
         >
            Bayar Sekarang
          </Button>
          </Box>
      </div>
    )
  }else{
    return (
      <div style={{ padding: '0% 3%' }} 
      //className='cartBerisi'
      >
        <form>
                <Grid >
              <Box sx={{flexGrow:1, margin:'10px 10px'}} display='flex'>
                <Box>
                  <Checkbox
                    style={{margin:'10px 10px'}} 
                    //id='checkboxAll'
                    className="form-check-input"
                   // onChange={selectAllCheckbox()}
                    //checked={listOfProduct.filter((item)=>item?.isChecked !== true).length<1}
                    //checked={!items.some((item) => item?.isChecked !== true)}
                     //onChange={handleChange} 
                    //  onChange={selectAllcbx()}
                    //onChange={(event) => selectAll(event.target.checked)}
                    onClick={(event) => selectAll(event.target.checked)}
                    checked={checkedAll}
                    ></Checkbox>
                Select All</Box>


                <Box sx={{ margin:'auto 30px auto auto'}}>
                  <Button 
                  onClick={handleClickOpenAll}
                  sx={{textTransform: 'capitalize', color:'black'}}>Delete All</Button>
                  <Dialog
                    open={openAll}
                    onClose={handleCloseAll}
                  >
                    <DialogTitle id="alert-dialog-title">
                      {"Apakah anda Benar ingin Menghapus seluruh barang belanjaan di keranjang anda?"}
                    </DialogTitle>
                    <DialogActions>
                      <Button onClick={handleCloseAll}>Tidak</Button>
                      <Button variant="contained" onClick={ async (e) => {await e.preventDefault(); await setallToDelete(); await deleteAll();}} autoFocus>
                        Iya
                      </Button>
                    </DialogActions>
                  </Dialog>

                </Box>
                 
              </Box>
              <div style={{ height: '0px', border: '1px solid grey' }}></div>
          </Grid>
        {listOfProduct.map((item,index) => (
        <Grid >
          <Grid >
              <Box sx={{ margin:'10px 10px' }} display='flex' key={index}>
                <Box display='inline-flex'>
                <Checkbox 
                style={{margin:'auto 10px'}} 
                className="form-check-input"

                //kita olah id nya di setiap perulangannya yakan ?


                //checked={item.status === 'included'}
                //id='select-option'
                //checked={item?.isChecked||true}
                 //onChange={selectAllcbx()}
                 onChange={() => toggleCheck(item.id)}
                checked={item.status === 'included'}
                onClick={() => {handleItemInclusion(item.id, item.status)} } />
                  <img src={`data:image/jpeg;base64,${item.image}`} width ='200'>
                  </img>
                </Box>
                <Box >
                  <Box>
                  <Typography sx={{margin:'40px auto auto 32px', alignItems:'flex-end', alignContent:'flex-end'}}>{item.name}</Typography>
                  </Box>
                  <Box sx={{margin:'100px auto auto 32px',  alignItems:'flex-end', alignContent:'flex-end'}}>
                  <Typography sx={{margin:'auto auto 0px 0px', alignItems:'flex-end', alignContent:'flex-end'}}>{numberFormat((item.total_price)) }</Typography>
                  </Box>
                
                
                </Box>
                <Box sx={{margin:'auto 0 auto auto',right:'0px', alignItems:'flex-end', alignContent:'flex-end'}}>
                  
                      <span sx={{margin:'10px 10px', alignItems:'flex-end', alignContent:'flex-end'}}>
                      { item.quantity != 1 ? 

                            <Button 
                            //onClick={ async (e) => {setQuantity((quantity=> quantity + 1));await e.preventDefault();await EditOrderInvoiceItem();}}
                            onClick={ async (e) => {await e.preventDefault(); setQuantity((quantity=> quantity - 1)); await handleQuantity(item.id, item.order_invoice_id, item.quantity, item.total_price, 'minus');}}
                             sx={{ 
                               right:'0', 
                               margin:'10px 10px', 
                               alignItems:'flex-end', 
                               alignContent:'flex-end'}}>-</Button>
                               :
                               <Button disabled variant="contained" sx={{ 
                                right:'0', 
                                margin:'10px 10px', 
                                alignItems:'flex-end', 
                                alignContent:'flex-end'}}>-</Button>}
                               </span>{item.quantity}<span>
                            <Button 
                            onClick={ async (e) => {await e.preventDefault(); setQuantity((quantity=> quantity + 1));await handleQuantity(item.id, item.order_invoice_id, item.quantity, item.total_price,'plus');}} 
                            sx={{
                              right:'0', 
                              margin:'10px 10px', 
                              alignItems:'flex-end', 
                              alignContent:'flex-end'}}>+</Button></span>
                          <div>
                          <IconButton 
                          onClick={ async (e) => {await e.preventDefault(); await setIdToDelete(item.id); await deleteItemFromCart();}}
                          //onClick={handleClickOpen} 
                          sx={{
                            left:'100px',
                             margin:'10px 10px', 
                             alignItems:'flex-end',
                             alignContent:'flex-end'}}><DeleteIcon /></IconButton>
                             {/* <Dialog
                                open={open}
                                onClose={handleClose}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                              >
                                <DialogTitle id="alert-dialog-title">
                                  {"Apakah anda Benar ingin Menghapus barang belanjaan ini dari keranjang anda?"}
                                </DialogTitle>
                                <DialogActions>
                                  <Button onClick={handleClose}>Tidak</Button>
                                  <Button onClick={ async (e) => {await e.preventDefault(); await setIdToDelete(item.id); await deleteItemFromCart();}} autoFocus>
                                    Iya
                                  </Button>
                                </DialogActions>
                              </Dialog> */}
                            </div>
                      
                </Box>
                  
              </Box>
              <div style={{ height: '0px', border: '1px solid grey' }}></div>
          </Grid>
        </Grid>
        ))}
        </form>


        {/* <Button 
        variant='contained' 
        component={Link} to="/checkout"  
        style={{margin: '30px 0px',textTransform: 'capitalize'}} 
        fullWidth 
        // onClick={() => {
        //   setOpenRincian(true);
        // }}

        > Check Out</Button> */}


        <Box
          display='flex'
      sx={{
        width: '98%',
        margin:'10px auto 0px auto',
        border: '1px solid grey',
        backgroundColor: 'white'
      }}
      padding='10px 10px'
      //position='fixed'
      bottom={0}
      
    >

        <Typography style={{width:'85%'}}>
        
          
          <Typography sx={{margin:'auto auto 10px 50px'}}>
            <h3>
              Rincian Belanjaan Total
            </h3>
          </Typography>
          
          <Typography display='flex' sx={{margin:'auto auto 10px 50px'}}>
            Jumlah Produk
            <Typography sx={{margin:'auto 0px 0px auto'}}>
            {sum}
            </Typography>
          </Typography> 
          <div style={{ height: '0px', border: '1px solid grey', margin:'0px 0px 0px 50px' }}></div>
          <Typography display='flex' sx={{margin:'0px 0px 0px 50px'}}>
          <h4>
          Total
          </h4>
          <Typography sx={{margin:'0px 0px auto auto'}}><h4>{numberFormat(sumTotalPrice)}</h4></Typography>
        </Typography>
       
        </Typography>
        
        <Button variant="contained"
         sx={{textTransform: 'capitalize',margin:'auto auto auto 20px'}}  
         href='checkout'
         >
            Bayar Sekarang
          </Button>
          </Box>


        {/* <Collapse in={openRincian}>

          <Paper elevation={5} display='flex'>
          <Typography width='1100px'>
          
          <Typography>
            <h3>
              Rincian Belanjaan Total
            </h3>
          </Typography>
          <Typography display='flex' sx={{margin:'auto auto 10px 0px'}}>
            Jumlah Produk
            <Typography sx={{margin:'auto 10px 0px auto'}}>{sum}</Typography>
          </Typography> 
          <div style={{ height: '0px', border: '1px solid grey' }}></div>
          <Typography display='flex' sx={{margin:'0px 0px auto auto'}}>
          <h4>
          Total
          </h4>
          <Typography sx={{margin:'0px 0px auto auto'}}><h4>{numberFormat(0)}</h4></Typography>
        </Typography>
        </Typography>
        <Button 
        // onClick={() => {
        //   setOpenRincian(false);
        // }}
        variant="contained" 
        href="cart" sx={{textTransform: 'capitalize',margin:'auto auto auto 20px'}}  >
            Bayar Sekarang
          </Button>

          </Paper>
              </Collapse> */}

        <div style={{ padding: "50px 50px" }}>
          <Box style={{
              width: '95%',
              height: 'auto',
              backgroundColor: "#FF7596",
              borderRadius: "20px",
              padding: '2%',
          }}>
              <Typography variant="h3" color="white"
                  style={{ fontSize: '25px', fontWeight: 'bold', padding: '0px 0 20px 0' }}>
                  Mungkin anda suka 
              </Typography>

                  <Carousel 
            swipeable={true}   
            draggable={true}
            responsive={responsive}
            autoPlay={true}
            >
                {listOfProducts.map((invoice) => (
                    <Grid item xs={6} md={4}component={Link} to={`/detail/${invoice.id}`} >
                    <Card style={{ height:'100%', borderRadius: '5%', marginRight:'2%'  }} variant="outlined" >
                    <CardMedia style={{ objectFit: 'contain'}} component="img" height="160" image={`data:image/jpeg;base64,${invoice.image}`} alt={invoice.image} />
                        <CardContent>

                            <Badge badgeContent={`Sold :${invoice.sold   }`} color="primary">
                            <Typography variant="h6" component="div">
                            {invoice.name}
                            </Typography>
                            </Badge>
                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                            {numberFormat(invoice.price) }
                            </Typography>
                            <Typography variant="body2">
                            {invoice.description}
                            <br />
                            </Typography>

                        </CardContent>
                        <CardActions className="newArrivalsCardActions">
                            <Button component={Link} to={`/detail/${invoice.id}`}className="newArrivalsCardAction" variant="contained" size="small">Detail</Button>

                        </CardActions>
                    </Card>
                    </Grid>
                ))}
            </Carousel>

          </Box>
      </div>
      </div>
    );
  }
  }

