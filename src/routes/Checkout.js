import { Button, Card, Input, InputLabel, MenuItem, Paper, Select, TextField, Typography } from "@mui/material";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { APIRequest } from "../components/APICalls";

export default function Checkout() {

  let navigate = useNavigate();
  
  /* useStates untuk keperluan POST checkout */
  // const [customerEmail, setCustomerEmail] = useState('');
  // const [customerPhone, setCustomerPhone] = useState('');
  // const [paymentMethod, setPaymentMethod] = useState('Bank BCA');
  // const [customerAddress, setCustomerAddress] = useState('');
  const [customerEmail, setCustomerEmail] = useState('exampleEmail@email.com');
  const [customerPhone, setCustomerPhone] = useState('081 123 456');
  const [paymentMethod, setPaymentMethod] = useState('Bank BCA');
  const [customerAddress, setCustomerAddress] = useState('Jalan Pederesan Kecil No.77, Kel. Kebonagung, Kec. Semarang Timur, Kota Semarang, Jawa Tengah, 50123.');
  /* useStates untuk keperluan POST checkout */

  /* Method to POST checkout */
  const checkout = async () => {
    const postDataa = {
        id: 0,
        customer_id: parseInt(JSON.parse(localStorage.getItem('loggedInUserId'))),
        customer_email: customerEmail,
        customer_phone: customerPhone,
        payment_method: paymentMethod,
        payment_verification_image: "",
        payment_total: 0,
        invoice_id: 0,
        customer_address: customerAddress,
        status: 'Checkout',
        created_at: "",
        updated_at:"",
        saveType: "edit"
    };
    /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
    APIRequest.defaults.headers.common['Authorization'] = 'bearer ' + JSON.parse(localStorage.getItem('loggedInUserAuthToken'));
    /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
    await APIRequest({ 
        method: 'POST', 
        url: 'api/OrderInvoice/checkout', 
        params: { orderInvoiceId: parseInt(JSON.parse(localStorage.getItem('pendingCartId'))) },
        data: postDataa,
    }).then((res) => {
        if (res.status === 200) {
            console.log(res.data)

            let pendingCartId = parseInt(JSON.parse(localStorage.getItem('pendingCartId')))
            if (res.data[0] = 'Temporary order_invoice has been made pending') {
              localStorage.setItem('pendingCartId', JSON.stringify(res.data.at(-1))); 
            } else {
              localStorage.removeItem("pendingCartId"); 
            }
            navigate(`/invoice-check/${pendingCartId}`);
        }
    }).catch((err) => {console.log(err.response.data)})
  }
  /* Method to POST checkout */

  return (
      <div>

          <Card elevation={8}  style={{ width: '80%', margin: '2% auto', padding:'2%'}} >
          <form  onSubmit={async (e) => { e.preventDefault(); await checkout();}}>

          <Typography style={{ textAlign: 'center'}} variant="h4" gutterBottom component="div">
          Checkout
          </Typography>

          <Typography style={{ textAlign: 'center'}} variant="subtitle2" gutterBottom component="div">
          Mohon lengkapi data terbaru anda demi memastikan pesanan anda sampai ke tujuan.
          </Typography>

          <Typography variant="h6" gutterBottom component="div">
          Kontak Pelanggan
          </Typography>

          <TextField 
          fullWidth
          placeholder="exampleEmail@email.com"
          //value={customerEmail}
          label="Email"
          onChange={(e) => setCustomerEmail(e.target.value)}
          style={{ marginBottom: '2%'}}
          />

          <TextField 
           fullWidth
           placeholder="081 123 456"
          //value={customerPhone}
          label="Telepon"
          onChange={(e) => setCustomerPhone(e.target.value)}
          style={{ marginBottom: '2%'}}
          />

          <Typography variant="h6" gutterBottom component="div">
          Metode Pembayaran
          </Typography>

          <Select
          labelId="demo-simple-select-label"
          fullWidth
          value={paymentMethod}
          label="Metode Pembayaran"
          onChange={(e) => setPaymentMethod(e.target.value)}
          style={{ marginBottom: '2%'}}
          >
          <MenuItem value="Bank BCA">Bank BCA</MenuItem>
          <MenuItem value="Bank BRI">Bank BRI</MenuItem>
          <MenuItem value="Bank Mandiri">Bank Mandiri</MenuItem>
          <MenuItem value="GoPay">GoPay</MenuItem>
          </Select>

          <Typography variant="h6" gutterBottom component="div">
          Alamat Penerima
          </Typography>

          <TextField 
           fullWidth
           placeholder="Jalan Pederesan Kecil No.77, Kel. Kebonagung, Kec. Semarang Timur, Kota Semarang, Jawa Tengah, 50123."
          //value={customerAddress}
          label="Alamat Penerima"
          onChange={(e) => setCustomerAddress(e.target.value)}
          style={{ marginBottom: '2%'}}
          />

          <Button fullWidth variant="contained" type="submit" >Checkout!!</Button>
        </form>
        </Card>

    </div>
  );
}