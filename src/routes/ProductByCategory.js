import {useParams} from "react-router-dom";
import { getNewArrivals } from "../jsonData/Data.js";
import { 
  Box, 
  ButtonGroup,
  Button, 
  Drawer, 
  Grid,
  Input,
  Typography
} from '@mui/material';

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { Link } from "react-router-dom";
import CardMedia from '@mui/material/CardMedia';
import Rating from '@mui/material/Rating';
import Badge from '@mui/material/Badge';
import SellIcon from '@mui/icons-material/Sell';
import 'react-multi-carousel/lib/styles.css';
import numberFormat from "../components/NumberFormat.js";
import { useState, useEffect } from "react";
import { APIRequest } from "../components/APICalls.js";
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import FilterListIcon from '@mui/icons-material/FilterList';

export default function ProductByCategory() {

     /* Mekanisme untuk toggle filter dan sort ketika menggunakan tampilan responsive */
     const [openFilterDrawer, setOpenFilterDrawer] = useState(false);
     const [openSortingDrawer, setOpenSortingDrawer] = useState(false);
      /* Mekanisme untuk toggle filter dan sort ketika menggunakan tampilan responsive */


    /* useStates dan metode-metode untuk keperluan GET daftar semua produk dari merk tertentu */
    let params = useParams();
    const [refreshPage, setRefreshPage] = useState(false);
    const [searchQuery, setSearchQuery] = useState();
    const [listOfProducts, setlistOfProducts] = useState([]);

    const [minPrice, setMinPrice] = useState('');
    const [maxPrice, setMaxPrice] = useState('');
    
    const [sortBy, setSortBy] = useState("Terbaru");

    const getlistOfProducts = async () => { await APIRequest({  method: 'GET', url: 'api/Product/list-products-by-category', params: { brandId: params.categoryId, minPrice: minPrice, maxPrice: maxPrice, sortBy:sortBy },
        }).then((res) => { if (res.status === 200) {  setlistOfProducts(res.data); } }).catch((err) => { })}
    useEffect(() => { getlistOfProducts(); }, [searchQuery, refreshPage, params.categoryId, minPrice, maxPrice,  sortBy])
    /* useStates untuk keperluan GET daftar semua produk dari merk tertentu */

    return (
      <div className="searchPageWrapper">

          {/* Drawer untuk fungsi filter merk dan harga */}        
          <div className="mobileFiltersAndSort" >
            <ButtonGroup style={{ marginBottom: '3%' }} fullWidth variant="outlined" aria-label="outlined primary button group">
                <Button onClick={()=> {setOpenFilterDrawer(true)}}><FilterAltIcon />Filter</Button>
                <Button onClick={()=> {setOpenSortingDrawer(true)}}><FilterListIcon />Urutkan</Button>
            </ButtonGroup>

            <Drawer anchor="bottom" width={600} open={openFilterDrawer} >
            <div style={{ padding: '5%'}} >
            <Typography style = {{ textTransform: 'capitalize' }} >
                        <h3 style={{ color: '#4F4F4F' }}>Filter</h3>
                    </Typography>

                    <Box sx={{ padding: '5% 8% 5% 8%', border: '1px solid hsl(1,1%,80%)', borderRadius: '25px',  height: 'auto' }} >
                        <Typography style = {{ textTransform: 'capitalize' }} >
                            <h4 style={{ color: '#4F4F4F', margin: '2% 0%' }}>Harga</h4>
                        </Typography>

                        <Typography >
                            Minimal : 
                        </Typography>
                        <Input
                        value={minPrice}
                        onChange={(e) => setMinPrice(e.target.value)}
                        style={{ marginBottom: '5%', width: '100%' }} name="productSearchMinimumPrice" placeholder="Rp" ></Input>

                        <Typography >
                            Maksimal : 
                        </Typography>
                        <Input 
                        value={maxPrice}
                        onChange={(e) => setMaxPrice(e.target.value)}
                        style={{ marginBottom: '5%' , width: '100%'}} name="productSearchMinimumPrice" placeholder="Rp" ></Input>
                        
                        <br/> 
                        <Button component={Link} to="/" variant="text" color="inherit" 
                        style = {{ 
                        width: '50%',
                        textTransform: 'capitalize',
                        color: '#FF7596',
                        border: '1px solid #FF7596' }}
                        >Pulihkan</Button>

                        <Button component={Link} to="/" variant="text" color="inherit" 
                        style = {{ 
                        width: '48%',
                        textTransform: 'capitalize',
                        color: 'white',
                        marginLeft: '2%',
                        backgroundColor: '#FF7596',
                        border: '1px solid #FF7596' }}
                        >Terapkan</Button>
                        </Box>

                        
                        <Button style={{ marginTop: '4%' }} fullWidth onClick={() => setOpenFilterDrawer(false)} variant="contained" 
                        >Terapkan</Button>

            
            </div>
            </Drawer>

            {/* Drawer untuk fungsi sorting */}        
            <Drawer anchor="bottom" width={600} open={openSortingDrawer} >
            <div style={{ padding: '5%'}} >

            <Typography style = {{ textTransform: 'capitalize' }} >
            <h3 style={{ color: '#4F4F4F' }}>Urutkan</h3>
            </Typography>
            
            <Grid container spacing={3} style={{ padding: '0 0 2% 0' }} >
                        <Grid item xs={6} md={6} >

                        <Button { ...(sortBy === 'Terbaru' ? {variant:"contained"} :{ variant:"outlined"})}  color="primary" 
                        style={{ width: '100%'}}
                        onClick={()=> { setSortBy("Terbaru")}}
                        >Terbaru</Button>

                        </Grid>

                        <Grid item xs={6} md={6} >

                        <Button  { ...(sortBy === 'Terlaris' ? {variant:"contained"} :{ variant:"outlined"})} color="primary" 
                        style={{ width: '100%'}}
                        onClick={()=> { setSortBy("Terlaris")}}
                        >Terlaris</Button>

                        </Grid>   

                        <Grid item xs={6} md={6} >

                        <Button  { ...(sortBy === 'Harga Tertinggi' ? {variant:"contained"} :{ variant:"outlined"})} color="primary"
                        style={{ width: '100%'}}
                        onClick={()=> { setSortBy("Harga Tertinggi")}}
                        >Harga Tertinggi</Button>

                        </Grid> 

                        <Grid item xs={6} md={6} >

                        <Button { ...(sortBy === 'Harga Terendah' ? {variant:"contained"} :{ variant:"outlined"})} color="primary" 
                        style={{ width: '100%'}}
                        onClick={()=> { setSortBy("Harga Terendah")}}
                        >Harga Terendah</Button>

                        </Grid>            
                    </Grid> 

                    <Button style={{ marginTop: '4%' }} fullWidth onClick={() => setOpenSortingDrawer(false)} variant="contained" 
                        >Terapkan</Button>
            
            </div>
            </Drawer>

            </div>

            {/* Drawer untuk fungsi sorting */}    

<Grid container spacing={3}
        style={{ padding: '0% 0' }}
    >
        <Grid item xs={12} md={3} >
        <div className="desktopFiltersAndSort" >
        <Typography style = {{ textTransform: 'capitalize' }} >
            <h3 style={{ color: '#4F4F4F' }}>Filter</h3>
        </Typography>

        <Box sx={{ padding: '2% 8% 2% 8%', border: '1px solid hsl(1,1%,60%)', borderRadius: '25px',  height: 'auto' }} >
            <Typography style = {{ textTransform: 'capitalize' }} >
                <h4 style={{ color: '#4F4F4F', margin: '2% 0%' }}>Harga</h4>
            </Typography>

            <Typography >
                Minimal : 
            </Typography>
            <Input 
            value={minPrice}
            onChange={(e) => setMinPrice(e.target.value)}
            style={{ marginBottom: '5%', width: '100%' }} name="productSearchMinimumPrice" placeholder="Rp" ></Input>

            <Typography >
                Maksimal : 
            </Typography>
            <Input 
            value={maxPrice}
            onChange={(e) => setMaxPrice(e.target.value)}
            style={{ marginBottom: '5%' , width: '100%'}} name="productSearchMinimumPrice" placeholder="Rp" ></Input>
            
            <br/> 
            <Button component={Link} to="/" variant="text" color="inherit" 
            style = {{ 
            width: '50%',
            textTransform: 'capitalize',
            color: '#FF7596',
            border: '1px solid #FF7596' }}
            >Pulihkan</Button>

            <Button component={Link} to="/" variant="text" color="inherit" 
            style = {{ 
            width: '48%',
            textTransform: 'capitalize',
            color: 'white',
            marginLeft: '2%',
            backgroundColor: '#FF7596',
            border: '1px solid #FF7596' }}
            >Terapkan</Button>


        </Box>
        </div>
        </Grid>

        <Grid item xs={12} md={9} >
        <div className="desktopFiltersAndSort" >
            
        <Typography style = {{ textTransform: 'capitalize' }} >
            <h3 style={{ color: '#4F4F4F' }}>Urutkan :</h3>
        </Typography>

        <Grid container spacing={3} style={{ padding: '2% 0' }} >
            <Grid item xs={12} md={3} >

                 <Button { ...(sortBy === 'Terbaru' ? {variant:"contained"} :{ variant:"outlined"})}  color="primary" 
                style={{ width: '100%'}}
                onClick={()=> { setSortBy("Terbaru")}}
                >Terbaru</Button>

                </Grid>

                <Grid item xs={12} md={3} >

                <Button  { ...(sortBy === 'Terlaris' ? {variant:"contained"} :{ variant:"outlined"})} color="primary" 
                style={{ width: '100%'}}
                onClick={()=> { setSortBy("Terlaris")}}
                >Terlaris</Button>

                </Grid>   

                <Grid item xs={12} md={3} >

                <Button  { ...(sortBy === 'Harga Tertinggi' ? {variant:"contained"} :{ variant:"outlined"})} color="primary"
                style={{ width: '100%'}}
                onClick={()=> { setSortBy("Harga Tertinggi")}}
                >Harga Tertinggi</Button>

                </Grid> 

                <Grid item xs={12} md={3} >

                <Button { ...(sortBy === 'Harga Terendah' ? {variant:"contained"} :{ variant:"outlined"})} color="primary" 
                style={{ width: '100%'}}
                onClick={()=> { setSortBy("Harga Terendah")}}
                >Harga Terendah</Button>
            </Grid>            
            </Grid>   
            </div>                  
            
            <Grid container spacing={3}  >

            {listOfProducts.map((invoice) => (
                <Grid item xs={6} md={4} component={Link} to={`/detail/${invoice.id}`} >
                    
                    <Card variant="outlined" >
                    <CardMedia style={{ objectFit: 'contain'}}  component="img" height="160" image={`data:image/jpeg;base64,${invoice.image}`} alt="green iguana" />
                        <CardContent>

                            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                            {invoice.number}
                            </Typography>
                            <Badge style={{ opacity: '0.8' }} badgeContent={`Sold :${invoice.sold   }`} color="primary">
                            <Typography variant="h6" component="div">
                            {invoice.name}
                            </Typography>
                            </Badge>
                            <Typography variant="subtitle2" color="text.secondary">
                            {numberFormat(invoice.price) }
                            </Typography>
                            <Typography variant="body2">
                            {invoice.description}
                            <br />
                            </Typography>

                            </CardContent>
                            <CardActions className="newArrivalsCardActions">
                            <Button component={Link} to={`/detail/${invoice.id}`} className="newArrivalsCardAction" variant="contained" size="small">Detail</Button>
                            </CardActions>
                    </Card>

                </Grid>
            ))}

        </Grid>


        </Grid>

</Grid>
      </div>
    );
  }