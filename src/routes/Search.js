import { AppBar, Badge, Box, Button, ButtonGroup, Card, CardActions, CardContent, CardMedia, Checkbox, Drawer, FormControlLabel, FormGroup, Grid, Input, MenuItem, Rating, Typography } from "@mui/material";
import SellIcon from '@mui/icons-material/Sell';
import { Link, useSearchParams } from "react-router-dom";
import numberFormat from "../components/NumberFormat";
import { useEffect, useState} from "react";
import Axios from 'axios';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import FilterListIcon from '@mui/icons-material/FilterList';

export const APIRequest = Axios.create({
    baseURL: 'https://localhost:7198/',
  });

export default function Search() {

    /* Mekanisme untuk mengambil data GET list-all-brands-name-and-id */
    const [listOfBrands, setListOfBrands] = useState('');
    const getListOfBrands = async () => { await APIRequest({  method: 'GET', url: 'api/Brand/list-all-brands-name-and-id', params: { },
    }).then((res) => { if (res.status === 200) { console.log("Success"); setListOfBrands(res.data); } }).catch((err) => { }) }
    useEffect(() => { getListOfBrands(); }, [])
    /* Mekanisme untuk mengambil data GET list-all-brands-name-and-id */

     /* Mekanisme untuk toggle filter dan sort ketika menggunakan tampilan responsive */
    const [openFilterDrawer, setOpenFilterDrawer] = useState(false);
    const [openSortingDrawer, setOpenSortingDrawer] = useState(false);
     /* Mekanisme untuk toggle filter dan sort ketika menggunakan tampilan responsive */

    /* Mekanisme untuk mengambil queri pencarian dan terapkan ke axios GET list-all-products */
    const [searchParams, setSearchParams] = useSearchParams();
    const [searchQuery, setSearchQuery] = useState(searchParams.get("search"));
    const [sortBy, setSortBy] = useState("Terbaru");
    const [brandIdsArray, setBrandIdsArray] = useState([]);
    const [brandIdsString, setBrandIdsString] = useState('');

    const handleBrandIds = (brandId) => {
        let tempArr = [...brandIdsArray]
        let idExistsAlready = false;
        tempArr.forEach( (eachBrandId) => {
            if (eachBrandId === brandId) {
                idExistsAlready = true;
            }
        })

        if(!idExistsAlready) {
            tempArr.push(brandId)
        } else {
            let indexToRemove = tempArr.indexOf(brandId)
            tempArr.splice(indexToRemove, 1);
        }
        setBrandIdsArray(tempArr)
    }
    const handleBrandIdsToString = () => {
        let text = brandIdsArray.join(",");
        setBrandIdsString(text)
    }
    useEffect(() => { handleBrandIdsToString() }, [brandIdsArray])
    useEffect(() => { console.log(brandIdsString) }, [brandIdsString])

    const [minPrice, setMinPrice] = useState('');
    const [maxPrice, setMaxPrice] = useState('');
    const [dataa, setDataa] = useState([]);
    const getData = () => { APIRequest({  method: 'GET', url: 'api/Product/list-all-products', params: { search: searchQuery, sortBy: sortBy,  brandIds: brandIdsString, minPrice: minPrice, maxPrice: maxPrice},
        }).then((res) => { if (res.status === 200) {  console.log(res.data);setDataa(res.data); } }).catch((err) => { }) }
    useEffect(() => { getData(); }, [searchQuery, minPrice, maxPrice, brandIdsString, sortBy])
    useEffect(() => { setSearchQuery(searchParams.get("search")); }, [searchParams])
    /* Mekanisme untuk mengambil queri pencarian dan terapkan ke axios GET list-all-products */

    return (
        <div className="searchPageWrapper">

        {/* Mulai untuk menampilkan komponen - komponen yang muncul di tampilan responsive ============================================== */}
        {/* Drawer untuk fungsi filter merk dan harga */}        
            <div className="mobileFiltersAndSort" >
            <ButtonGroup style={{ marginBottom: '3%' }} fullWidth variant="outlined" aria-label="outlined primary button group">
                <Button onClick={()=> {setOpenFilterDrawer(true)}}><FilterAltIcon />Filter</Button>
                <Button onClick={()=> {setOpenSortingDrawer(true)}}><FilterListIcon />Urutkan</Button>
            </ButtonGroup>

            <Drawer anchor="bottom" width={600} open={openFilterDrawer} >
            <div style={{ padding: '5%'}} >
            <Typography style = {{ textTransform: 'capitalize' }} >
                        <h3 style={{ color: '#4F4F4F' }}>Filter</h3>
                    </Typography>

                    <Box sx={{ padding: '5% 8% 5% 8%', border: '1px solid hsl(1,1%,80%)', borderRadius: '25px',  height: 'auto' , marginBottom: '5%'}} >
                        <Typography style = {{ textTransform: 'capitalize' }} >
                            <h4 style={{ color: '#4F4F4F', margin: '2% 0%' }}>Merk</h4>
                        </Typography>
                        <FormGroup>
                        <Grid container spacing={1} >
                        {Object.entries(listOfBrands).map(([ key, brand ], i) =>  (
                            <Grid item xs={6} md={6} >
                            <FormControlLabel control={<Checkbox onClick={() => { handleBrandIds(brand.id)}} />} label={brand.brand_name} />
                            </Grid>
                        ))}
                        </Grid>
                        </FormGroup>
                    </Box>

                    <Box sx={{ padding: '5% 8% 5% 8%', border: '1px solid hsl(1,1%,80%)', borderRadius: '25px',  height: 'auto' }} >
                        <Typography style = {{ textTransform: 'capitalize' }} >
                            <h4 style={{ color: '#4F4F4F', margin: '2% 0%' }}>Harga</h4>
                        </Typography>

                        <Typography >
                            Minimal : 
                        </Typography>
                        <Input
                        value={minPrice}
                        onChange={(e) => setMinPrice(e.target.value)}
                        style={{ marginBottom: '5%', width: '100%' }} name="productSearchMinimumPrice" placeholder="Rp" ></Input>

                        <Typography >
                            Maksimal : 
                        </Typography>
                        <Input 
                        value={maxPrice}
                        onChange={(e) => setMaxPrice(e.target.value)}
                        style={{ marginBottom: '5%' , width: '100%'}} name="productSearchMinimumPrice" placeholder="Rp" ></Input>
                        
                        <br/> 
                        <Button  variant="text" color="inherit" 
                        style = {{ 
                        width: '50%',
                        textTransform: 'capitalize',
                        color: '#FF7596',
                        border: '1px solid #FF7596' }}
                        >Pulihkan</Button>

                        <Button  variant="text" color="inherit" 
                        style = {{ 
                        width: '48%',
                        textTransform: 'capitalize',
                        color: 'white',
                        marginLeft: '2%',
                        backgroundColor: '#FF7596',
                        border: '1px solid #FF7596' }}
                        >Terapkan</Button>
                        </Box>

                        
                        <Button style={{ marginTop: '4%' }} fullWidth onClick={() => setOpenFilterDrawer(false)} variant="contained" 
                        >Terapkan</Button>

            
            </div>

            
            </Drawer>

            {/* Drawer untuk fungsi sorting */}        
            <Drawer anchor="bottom" width={600} open={openSortingDrawer} >
            <div style={{ padding: '5%'}} >

            <Typography style = {{ textTransform: 'capitalize' }} >
            <h3 style={{ color: '#4F4F4F' }}>Urutkan</h3>
            </Typography>
            
            <Grid container spacing={3} style={{ padding: '0 0 2% 0' }} >
                        <Grid item xs={6} md={6} >

                        <Button { ...(sortBy === 'Terbaru' ? {variant:"contained"} :{ variant:"outlined"})}  color="primary" 
                        style={{ width: '100%'}}
                        onClick={()=> { setSortBy("Terbaru")}}
                        >Terbaru</Button>

                        </Grid>

                        <Grid item xs={6} md={6} >

                        <Button  { ...(sortBy === 'Terlaris' ? {variant:"contained"} :{ variant:"outlined"})} color="primary" 
                        style={{ width: '100%'}}
                        onClick={()=> { setSortBy("Terlaris")}}
                        >Terlaris</Button>

                        </Grid>   

                        <Grid item xs={6} md={6} >

                        <Button  { ...(sortBy === 'Harga Tertinggi' ? {variant:"contained"} :{ variant:"outlined"})} color="primary"
                        style={{ width: '100%'}}
                        onClick={()=> { setSortBy("Harga Tertinggi")}}
                        >Harga Tertinggi</Button>

                        </Grid> 

                        <Grid item xs={6} md={6} >

                        <Button { ...(sortBy === 'Harga Terendah' ? {variant:"contained"} :{ variant:"outlined"})} color="primary" 
                        style={{ width: '100%'}}
                        onClick={()=> { setSortBy("Harga Terendah")}}
                        >Harga Terendah</Button>

                        </Grid>            
                    </Grid> 

                    <Button style={{ marginTop: '4%' }} fullWidth onClick={() => setOpenSortingDrawer(false)} variant="contained" 
                        >Terapkan</Button>
            
            </div>
            </Drawer>
            </div>
        {/* Selesai untuk menampilkan komponen - komponen yang muncul di tampilan responsive ============================================== */}

        {/* Mulai untuk menampilkan komponen - komponen yang muncul di tampilan desktop ============================================== */}
            Hasil ditemukan : <strong>{searchParams.get("search")}</strong>

            
            <Grid container spacing={3}
                    style={{  }}
                >
                    
                    <Grid item xs={12} md={3} >
                    <div className="desktopFiltersAndSort" >
                    <Typography style = {{ textTransform: 'capitalize' }} >
                        <h3 style={{ color: '#4F4F4F' }}>Filter</h3>
                    </Typography>

                    <Box sx={{ padding: '5% 8% 5% 8%', border: '1px solid hsl(1,1%,80%)', borderRadius: '25px',  height: 'auto' , marginBottom: '5%'}} >
                        <Typography style = {{ textTransform: 'capitalize' }} >
                            <h4 style={{ color: '#4F4F4F', margin: '2% 0%' }}>Merk</h4>
                        </Typography>



                        <FormGroup>
                            
                        {Object.entries(listOfBrands).map(([ key, brand ], i) =>  (
                            <FormControlLabel control={<Checkbox onClick={() => { handleBrandIds(brand.id)}} />} label={brand.brand_name} />
                        ))}
                        </FormGroup>


                    </Box>

                    <Box sx={{ padding: '5% 8% 5% 8%', border: '1px solid hsl(1,1%,80%)', borderRadius: '25px',  height: 'auto' }} >
                        <Typography style = {{ textTransform: 'capitalize' }} >
                            <h4 style={{ color: '#4F4F4F', margin: '2% 0%' }}>Harga</h4>
                        </Typography>

                        <Typography >
                            Minimal : 
                        </Typography>
                        <Input
                        value={minPrice}
                        onChange={(e) => setMinPrice(e.target.value)}
                        style={{ marginBottom: '5%', width: '100%' }} name="productSearchMinimumPrice" placeholder="Rp" ></Input>

                        <Typography >
                            Maksimal : 
                        </Typography>
                        <Input 
                        value={maxPrice}
                        onChange={(e) => setMaxPrice(e.target.value)}
                        style={{ marginBottom: '5%' , width: '100%'}} name="productSearchMinimumPrice" placeholder="Rp" ></Input>
                        
                        <br/> 
                        <Button  variant="text" color="inherit" 
                        style = {{ 
                        width: '50%',
                        textTransform: 'capitalize',
                        color: '#FF7596',
                        border: '1px solid #FF7596' }}
                        >Pulihkan</Button>

                        <Button  variant="text" color="inherit" 
                        style = {{ 
                        width: '48%',
                        textTransform: 'capitalize',
                        color: 'white',
                        marginLeft: '2%',
                        backgroundColor: '#FF7596',
                        border: '1px solid #FF7596' }}
                        >Terapkan</Button>


                    </Box>
                     </div>
                    </Grid>
                   

                    <Grid item xs={12} md={9} >

                    <div className="desktopFiltersAndSort" >
                    <Typography style = {{ textTransform: 'capitalize' }} >
                        <h3 style={{ color: '#4F4F4F' }}>Urutkan</h3>
                    </Typography>
                    <Grid container spacing={3} style={{ padding: '0 0 2% 0' }} >
                        <Grid item xs={12} md={3} >

                        <Button { ...(sortBy === 'Terbaru' ? {variant:"contained"} :{ variant:"outlined"})}  color="primary" 
                        style={{ width: '100%'}}
                        onClick={()=> { setSortBy("Terbaru")}}
                        >Terbaru</Button>

                        </Grid>

                        <Grid item xs={12} md={3} >

                        <Button  { ...(sortBy === 'Terlaris' ? {variant:"contained"} :{ variant:"outlined"})} color="primary" 
                        style={{ width: '100%'}}
                        onClick={()=> { setSortBy("Terlaris")}}
                        >Terlaris</Button>

                        </Grid>   

                        <Grid item xs={12} md={3} >

                        <Button  { ...(sortBy === 'Harga Tertinggi' ? {variant:"contained"} :{ variant:"outlined"})} color="primary"
                        style={{ width: '100%'}}
                        onClick={()=> { setSortBy("Harga Tertinggi")}}
                        >Harga Tertinggi</Button>

                        </Grid> 

                        <Grid item xs={12} md={3} >

                        <Button { ...(sortBy === 'Harga Terendah' ? {variant:"contained"} :{ variant:"outlined"})} color="primary" 
                        style={{ width: '100%'}}
                        onClick={()=> { setSortBy("Harga Terendah")}}
                        >Harga Terendah</Button>

                        </Grid>     
                        
                    </Grid>                     
                    </div>
        {/* Selesai untuk menampilkan komponen - komponen yang muncul di tampilan desktop ============================================== */}       

                        <Grid container spacing={3}  >
                        { (dataa.length === 0) ?
                        <Grid item xs={12} md={12} >
                        <div style={{ margin: '0px auto', textAlign: 'center'}}>
                        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                        Maaf, pencarian tidak ditemukan
                        </Typography>
                        </div>
                        </Grid>
                        :
                        <></>
                    
                        }

                        {Object.entries(dataa).map(([ key, invoice ], i) =>  (
                            <Grid item xs={6} md={4}component={Link} to={`/detail/${invoice.id}`} >
                                
                                <Card variant="outlined" >
                                <CardMedia style={{ objectFit: 'contain'}}  component="img" height="160" image={`data:image/jpeg;base64,${invoice.image}`} alt="" />
                                    <CardContent>


                                        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                                        {invoice.number}
                                        </Typography>
                                        <Badge style={{ opacity: '0.8' }} badgeContent={`Sold :${invoice.sold   }`} color="primary">
                                        <Typography variant="h6" component="div">
                                        {invoice.name}
                                        </Typography>
                                        </Badge>
                                        <Typography variant="subtitle2" color="text.secondary">
                                        {numberFormat(invoice.price) }
                                        </Typography>
                                        <Typography variant="body2">
                                        {invoice.description}
                                        <br />
                                        </Typography>

                                        </CardContent>
                                        <CardActions className="newArrivalsCardActions">
                                        <Button component={Link} to={`/detail/${invoice.id}`} className="newArrivalsCardAction" variant="contained" size="small">Detail</Button>
                                        </CardActions>
                                </Card>

                            </Grid>
                        ))}

                    </Grid>


                    </Grid>

            </Grid>
      </div>
    );
  }