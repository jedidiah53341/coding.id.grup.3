import { Avatar, Button, Checkbox, FormControlLabel, Grid, Link, Paper, TextField, Typography } from '@mui/material'
import React, { useState } from 'react';
import { pink } from '@mui/material/colors';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { APIRequest } from '../components/APICalls';
import { useNavigate } from 'react-router-dom';

const Login = () => {

    let navigate = useNavigate();

    /* useStates untuk keperluan POST login */
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    /* useStates untuk keperluan POST login */

    /* Method to POST login */
    const login = () => {
        const postDataa = { username: username, customer_name: "", email: "", password: password, role: "" };
        APIRequest({
            method: 'POST', url: 'api/Login/Login', data: postDataa,
        }).then((res) => {
            if (res.status === 200) {

                console.log(res.data)
                localStorage.setItem('loggedInUserId', JSON.stringify(res.data[0]));
                localStorage.setItem('loggedInUserRole', JSON.stringify(res.data[1]));

                if (res.data[2] === "Pending invoice exists!") {
                    localStorage.setItem('pendingCartId', JSON.stringify(res.data[3]));
                    console.log("pending cart stored to Local Storage.")
                    localStorage.setItem('loggedInUserAuthToken', JSON.stringify(res.data[4]));
                    console.log("auth token stored to Local Storage.")
                } else {
                    localStorage.setItem('loggedInUserAuthToken', JSON.stringify(res.data[2]));
                    console.log("auth token stored to Local Storage.")
                }
                navigate("/");
            }
        }).catch((err) => { alert('LOGIN FAILED: ' + err.response.data) })

    }

    /* Method to POST login */
    const paperStyle = { padding: 20, height: '70vh', width: 290, margin: '0px auto' }
    const btstyle = { margin: '8px 0' }
    return (
        <div fullWidth
            style={{
                background: '#FF7596', padding: '6%', height: '100%' 
            }} >
            <Grid >
                <Paper elevation={20} className="loginForm" >
                    <Grid align='center'>
                        <Avatar sx={{ bgcolor: pink[500] }}><LockOutlinedIcon /></Avatar>
                        <h1>Sign In</h1>

                        <Link href="/" style={{ textDecoration: 'none', color: '#FF7596' }}>
                            <Typography variant="h6" style={{paddingBottom:'10px', marginTop:'-15px'}}> 1Electronic </Typography>
                        </Link>
                    </Grid>
                    <form onSubmit={(e) => { e.preventDefault(); login() }} >
                        <TextField value={username} onChange={(e) => setUsername(e.target.value)} label='Username' placeholder='Enter Username' variant='outlined' fullWidth required /><br/><br/>
                        <TextField value={password} onChange={(e) => setPassword(e.target.value)} label='Password' placeholder='Enter Password' type='password' variant='outlined' fullWidth required />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name='checked'
                                    color='primary'
                                />
                            }
                            label='Remember Me'
                        />
                        <Button type='submit' variant='contained' color='primary' fullWidth style={btstyle}>Sign In</Button>
                    </form>
                    <Typography >
                        <Link style={{ textDecoration: 'none', }} href="#">Forgot Password ?</Link>
                    </Typography>
                    <Typography >
                        <Link style={{ textDecoration: 'none', }} href="/register">Sign Up</Link>
                    </Typography>
                </Paper>
            </Grid>
        </div>
    )
}

export default Login
