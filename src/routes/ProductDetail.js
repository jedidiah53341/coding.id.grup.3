import { useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { getNewArrivals } from "../jsonData/Data.js";
import {
    Alert,
    Box,
    Button,
    Grid,
    Typography
} from '@mui/material';

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { Link } from "react-router-dom";
import CardMedia from '@mui/material/CardMedia';
import Rating from '@mui/material/Rating';
import Badge from '@mui/material/Badge';
import SellIcon from '@mui/icons-material/Sell';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import numberFormat from "../components/NumberFormat.js";
import { APIRequest } from "../components/APICalls.js";
import { detailProductResponsive } from "../components/ReactMultiCarousel";

export default function ProductDetail() {

    let navigate = useNavigate();

    let params = useParams();
    const [quantity, setQuantity] = useState(1);
    const [subTotal, setSubTotal] = useState(0);

    /* useStates dan metode-metode untuk keperluan GET detail dari sebuah produk */
    const [openAlert, setOpenAlert] = useState(false);
    /* useStates untuk keperluan GET detail dari sebuah produk */

    /* useStates dan metode-metode untuk keperluan GET detail dari sebuah produk */
    const [detailOfAProduct, setDetailOfAProduct] = useState([]);
    const getdetailOfAProduct = async () => {
        await APIRequest({
            method: 'GET', url: 'api/Product/product-detail', params: { productId: params.productId },
        }).then((res) => { if (res.status === 200) { setDetailOfAProduct(res.data[0]); } }).catch((err) => { })
    }
    useEffect(() => { getdetailOfAProduct(); }, [params])
    /* useStates untuk keperluan GET detail dari sebuah produk */


    /* useStates dan metode-metode untuk keperluan GET daftar seluruh produk */
    const [listOfProducts, setlistOfProducts] = useState([]);
    const getlistOfProducts = async () => {
        await APIRequest({
            method: 'GET', url: 'api/Product/list-all-products', params: { productId: "" },
        }).then((res) => { if (res.status === 200) { setlistOfProducts(res.data); } }).catch((err) => { })
    }
    useEffect(() => { getlistOfProducts(); }, [params])
    /* useStates dan metode-metode untuk keperluan GET daftar seluruh produk */

    /* Method to POST new Order Invoice Item */
    const postNewOrderInvoiceItem = () => {
        const postDataa = { order_invoice_id: 0, product_id: detailOfAProduct.id, status: "included", quantity: quantity, total_price: subTotal, created_at: "", updated_at: "", image: "", name: "", saveType: "add" };
        APIRequest({
            method: 'POST', url: 'api/OrderInvoiceItem/post-order-invoice-item', data: postDataa, params: { loggedInUserId: JSON.parse(localStorage.getItem('loggedInUserId')) },
        }).then((res) => {
            if (res.status === 200) {
                setOpenAlert(true);
                localStorage.setItem('pendingCartId', JSON.stringify(res.data.at(-1)));
                setTimeout(() => setOpenAlert(false), 2000);
            }
        }).catch((err) => { console.log(err.response.data) })
    }
    /* Method to POST Order Invoice Item */

    const postNewOrderInvoiceItemAndCheckoutNow = async () => {
        await postNewOrderInvoiceItem()
        await navigate(`/cart`);
    }

    useEffect(() => {
        setSubTotal(detailOfAProduct.price * quantity)
    }, [quantity, detailOfAProduct]);

    useEffect(() => {
        setQuantity(1)
    }, [detailOfAProduct]);


    return (
        <div className="productDetailWrapper" style={{ color: '#4F4F4F' }}>

            <div className="productDetailContent">
                {/* Alert yang ditampilkan ketika pelanggan menambahkan item */}
                {openAlert === true ?
                    <Alert className="success-alert" variant="filled" severity="success">
                        Produk berhasil ditambahkan ke keranjang!
                    </Alert>
                    :
                    <></>
                }

                <Grid container columnGap="10px" justifyContent="center" alignContent="center"
                    style={{ paddingBottom: '50px' }} >
                    <Grid >
                        <Box display="flex" width="400px" height="100%"
                            style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <img src={`data:image/jpeg;base64,${detailOfAProduct.image}`}
                                width="250px" alt={detailOfAProduct.image}>
                            </img>
                        </Box>
                    </Grid>
                    <Grid >
                        <Box sx={{ height: 300 }} >
                            <Grid
                                style={{ paddingBottom: '100px' }}>
                                <Badge badgeContent={'Sold : ' + detailOfAProduct.sold} color="primary">
                                    <Typography variant='h3' fontWeight="100" style={{ marginBottom: '-10px' }}>
                                        {detailOfAProduct.name}
                                    </Typography>
                                </Badge>
                                <h2>
                                    {numberFormat(detailOfAProduct.price)}
                                </h2>
                            </Grid>
                            <Grid >
                                <div ></div>
                                <Typography variant='body1' fontWeight="bold">
                                    Jumlah
                                    <span style={{ paddingLeft: '30px' }}>
                                        <Button onClick={() => setQuantity((quantity => quantity - 1))} >-</Button></span>{quantity}<span>
                                        <Button onClick={() => setQuantity((quantity => quantity + 1))} >+</Button></span>
                                </Typography>
                                <br />
                                <Typography variant='body1' fontWeight="bold">
                                    Sub Total
                                    <span style={{ paddingLeft: '30px' }}>{numberFormat(subTotal)}</span>
                                </Typography>
                            </Grid>
                        </Box>
                    </Grid>
                    <Grid >
                        <Box sx={{
                            width: 200,
                            height: 300,
                            // paddingTop: '80%',
                            // marginBottom: '-100%'
                        }}
                        > {/* style={{ paddingBottom: '20px' }} */}
                            {JSON.parse(localStorage.getItem('loggedInUserRole')) != null ?
                                <Grid paddingTop="200px">
                                    <Button variant="contained"
                                        onClick={async (e) => { await e.preventDefault(); await postNewOrderInvoiceItem(); }}
                                        style={{
                                            color: '#FFFFFF',
                                            textTransform: 'capitalize',
                                            backgroundColor: '#FF7596',
                                            width: '100%',
                                        }}
                                    >
                                        Masuk Keranjang
                                    </Button>

                                    <div style={{ paddingTop: '15px' }}>
                                        <Button
                                            onClick={async (e) => { await e.preventDefault(); await postNewOrderInvoiceItemAndCheckoutNow(); }}
                                            variant="contained"
                                            style={{
                                                color: '#FF7596',
                                                textTransform: 'capitalize',
                                                backgroundColor: '#FFFFFF',
                                                width: '100%',
                                                border: '2px solid #FF7596'
                                            }}
                                        >
                                            Beli Sekarang
                                        </Button>
                                    </div>
                                </Grid>
                                :
                                <div>

                                    <Grid paddingTop="200px">
                                        <Button variant="outlined"
                                        >
                                            Silahkan login untuk membeli
                                        </Button>
                                    </Grid>
                                </div>
                            }
                        </Box>
                    </Grid>
                </Grid>

                <Typography variant="h3" style={{ fontSize: '25px', fontWeight: 'bold', paddingBottom: '10px' }}>
                    Detail
                </Typography>
                <Typography component={Link} to={`/category/${detailOfAProduct.brand_id}`} style={{ color: "#4F4F4F" }}>
                    Merek : <span style={{ fontWeight: 'bold' }}>{detailOfAProduct.brand_name}</span><br /><br />
                </Typography>
                <Typography variant="h3" style={{ fontSize: '25px', paddingBottom: '40px' }}>

                    {detailOfAProduct.description}
                </Typography>

                <div style={{ height: '0px', border: '1px solid grey' }}></div>
            </div>


            <div className="andaMungkinJugaSuka" >
                <Box style={{
                    width: '80%',
                    height: 'auto',
                    backgroundColor: "#FF7596",
                    borderRadius: "20px",
                    padding: '2%',
                    margin: '0px auto'
                }}>
                    <Typography variant="h3" color="white"
                        style={{ fontSize: '25px', fontWeight: 'bold', padding: '0px 0 20px 0' }}>
                        Mungkin anda suka
                    </Typography>

                    <Carousel
                        swipeable={true}

                        draggable={true}
                        responsive={detailProductResponsive}
                        autoPlay={true}
                    >
                        {listOfProducts.map((invoice) => (
                            <Grid component={Link} to={`/detail/${invoice.id}`} >
                            <Card style={{ height: '100%', borderRadius: '5%', marginRight: '2%' }} variant="outlined" >
                                <CardMedia style={{ objectFit: 'contain' }} component="img" height="160" image={`data:image/jpeg;base64,${invoice.image}`} alt={invoice.image} />
                                <CardContent>
                                    <Badge style={{ opacity: '0.8' }} badgeContent={`Sold :${invoice.sold}`} color="primary">
                                        <Typography variant="h6" component="div">
                                            {invoice.name}
                                        </Typography>
                                    </Badge>
                                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                                        {numberFormat(invoice.price)}
                                    </Typography>
                                    <Typography variant="body2">
                                        {invoice.description}
                                        <br />
                                    </Typography>

                                </CardContent>
                                <CardActions className="newArrivalsCardActions">
                                    <Button component={Link} to={`/detail/${invoice.id}`} className="newArrivalsCardAction" variant="contained" size="small">Detail</Button>

                                </CardActions>
                            </Card>
                            </Grid>
                        ))}
                    </Carousel>

                </Box>
            </div>
        </div>
    );
}