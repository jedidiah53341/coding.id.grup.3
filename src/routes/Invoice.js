import { useEffect, useState } from "react";
import {
  Button, Box, Typography, TableContainer, Paper, Table, TableHead, TableRow, TableBody, Grid
} from "@mui/material";
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import { styled } from '@mui/material/styles';

import { APIRequest } from "../components/APICalls";
import numberFormat from "../components/NumberFormat";
import { Link } from "react-router-dom";

function Invoice() {
  const [openRincianInvoice, setOpenRincianInvoice] = useState(false);

  /* useStates dan metode-metode untuk keperluan GET daftar semua invoice =======================================*/
  // const [refreshPage, setRefreshPage] = useState(false);
  const [listOfInvoices, setlistOfInvoices] = useState([]);
  const getlistOfInvoices = async () => {
    /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
    APIRequest.defaults.headers.common['Authorization'] = 'bearer ' + JSON.parse(localStorage.getItem('loggedInUserAuthToken'));
    await APIRequest({
      method: 'POST',
      url: 'api/OrderInvoice/order-invoice-by-user',
      params: { userId: JSON.parse(localStorage.getItem('loggedInUserId')) },
    }).then((res) => {
      if (res.status === 200) {
        console.log(res.data); 
        setlistOfInvoices(res.data);
      }
    }).catch((err) => { })
  }
  useEffect(() => { getlistOfInvoices(); }, [])
  /* useStates untuk keperluan GET daftar semua invoice ====================================================*/

  const [invoceId, setInvoceId] = useState(0);
  const [noInvoce, setNoInvoce] = useState('');
  const [totalHarga, setTotalHarga] = useState(0);
  const [totalProduct, setTotalProduct] = useState(0);

  /* useStates dan metode-metode untuk keperluan GET daftar semua order invoice items ===================================*/
  const [listOfOrderInvoiceItem, setlistOfOrderInvoiceItem] = useState([]);
  const getlistOfOrderInvoiceItem = async () => {
    await APIRequest({
      method: 'POST',
      url: 'api/OrderInvoiceItem/list-all-order-invoice-items',
      params: { orderInvoiceId: invoceId },
    }).then((res) => {
      if (res.status === 200) {
        console.log(res.data); setlistOfOrderInvoiceItem(res.data);
      }
    }).catch((err) => { })
  }
  useEffect(() => { getlistOfOrderInvoiceItem(); }, [invoceId])
  /* useStates untuk keperluan GET daftar semua order invoice items ================================================*/

  // STYLE TABLE INVOICE =======================================================================================
  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: "#E0E0E0",
      color: "#4F4F4F"
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(even)': {
      backgroundColor: "#FF759633",
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));
  // STYLE TABLE INVOICE ===========================================================================================

  return (
    <div fullWidth height="100%" style={{ color: "#4F4F4F", padding: "40px 50px" }}>
      {openRincianInvoice ?
        <div>
          <Typography display="flex">
            <Typography component={Link} to="/" paddingRight={1} color="#4F4F4F">Beranda</Typography> &gt;
            <Typography component={Link} to="" paddingLeft={1} paddingRight={1} color="#4F4F4F"
              onClick={() => { openRincianInvoice(false) }}>
              Invoice
            </Typography> &gt;
            <Typography fontWeight="bold" paddingLeft={1}>Rincian Invoice</Typography>
          </Typography>
          <br />
          <Typography fontWeight="bold">
            Rincian Invoice
          </Typography>
          <br />
          <Typography display="flex">
            No. Invoice : <Typography paddingLeft={5}>{noInvoce}</Typography>
          </Typography>
          <Typography >
            Tanggal Beli :
            <br/><br/>
            <Typography className="invoiceTotalHarga" variant="h6" fontWeight="bold">
              Total Harga <Typography style={{ display: 'inline' }} variant="h6" >{numberFormat(totalHarga)}</Typography>
            </Typography>
            <Typography variant="h6"></Typography>
          </Typography>
        </div> :
        <div>
          <Typography display="flex">
            {/* <Typography component={Link} to="/" variant="Button" 
              style={{ color: "black", textTransform: 'capitalize'}} >
              Beranda &gt;
            </Typography> */}
            <Typography component={Link} to="/" paddingRight={1} color="#4F4F4F">Beranda</Typography> &gt;
            <Typography fontWeight="bold" paddingLeft={1}>Invoice</Typography>
          </Typography>
          <br />
          <Typography fontWeight="bold">
            Invoice Menu
          </Typography>
        </div>
      }
      <br />
      <TableContainer component={Paper} sytle={{ backgroundColor: "grey" }}>
        <Table fullWidth>
          <TableHead >
            <TableRow>
              <StyledTableCell align="center">No</StyledTableCell>
              <StyledTableCell align="center">
                {openRincianInvoice ? "Nama Produk" : "No. Invoice"}
              </StyledTableCell>
              <StyledTableCell align="center">
                {openRincianInvoice ? "Merek" : "Tanggal"}
              </StyledTableCell>
              <StyledTableCell align="center">Total Produk</StyledTableCell>
              <StyledTableCell align="center">Total Harga</StyledTableCell>
              {openRincianInvoice ? "" : <StyledTableCell align="center">Action</StyledTableCell>}
            </TableRow>
          </TableHead>
          {openRincianInvoice ?
            <TableBody>
              {/* Rincian Invoice */}
              {listOfOrderInvoiceItem.map((data, index) => (
                <StyledTableRow>
                  <StyledTableCell align="center">{index + 1}</StyledTableCell>
                  <StyledTableCell align="center">{data.name}</StyledTableCell>
                  <StyledTableCell align="center">{data.brand_name}</StyledTableCell>
                  <StyledTableCell align="center">{data.quantity}</StyledTableCell>
                  <StyledTableCell align="center">{numberFormat(data.total_price)}</StyledTableCell>
                </StyledTableRow>
              ))};
            </TableBody>
            :
            <TableBody>
              {/* Invoice */}
              {listOfInvoices.map((data, index) => (
                <StyledTableRow>
                  <StyledTableCell align="center">{index + 1}</StyledTableCell>
                  <StyledTableCell align="center">{data.invoice_number}</StyledTableCell>
                  <StyledTableCell align="center">{data.created_at}</StyledTableCell>
                  <StyledTableCell align="center">{data.product_total_quantity}</StyledTableCell>
                  <StyledTableCell align="center">{numberFormat(data.payment_total)}</StyledTableCell>
                  <StyledTableCell align="center" >

                    <Grid container spacing={1}>
                      <Grid item xs={12} md={6}>
                          <Button fullWidth variant="contained"
                          style={{textTransform: "capitalize", backgroundColor: "#FF7596" }}
                          onClick={() => {
                            setOpenRincianInvoice(true)
                            setInvoceId(data.id)
                            setNoInvoce(data.invoice_number)
                            setTotalHarga(data.payment_total)
                            // console.log(noInvoce)
                          }}>
                          Rincian
                        </Button>
                      </Grid>
                      <Grid item xs={12} md={6}>
                          <Button fullWidth component={Link} to={`/invoice-check/${data.id}`} variant="contained"
                          style={{ marginLeft: '2%', backgroundColor: "#FF7596", textTransform: "capitalize" }}>
                          Cek Status
                          </Button>
                      </Grid>
                    </Grid>
                    



                  </StyledTableCell>
                </StyledTableRow>
              ))};
            </TableBody>
          }
        </Table>
      </TableContainer>

    </div>
  );
}

export default Invoice