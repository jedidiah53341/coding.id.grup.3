import {Link, useNavigate, useParams} from "react-router-dom";
import 'react-multi-carousel/lib/styles.css';
import numberFormat from "../components/NumberFormat.js";
import { useState, useEffect } from "react";
import { APIRequest } from "../components/APICalls.js";
import { Alert, Button, Card, Divider, FormControlLabel, FormGroup, Paper, Typography, Checkbox, Grid } from "@mui/material";
import ChevronRightIcon from '@mui/icons-material/ChevronRight';

export default function InvoiceCheck() {

    let navigate = useNavigate();

    /* useStates dan metode-metode untuk keperluan GET detail invoice */
    let params = useParams();
    const [invoiceDetail, setInvoiceDetail] = useState([]);
    const getInvoiceDetail = async () => { 
    /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
    APIRequest.defaults.headers.common['Authorization'] = 'bearer ' + JSON.parse(localStorage.getItem('loggedInUserAuthToken'));
    /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
    await APIRequest({  method: 'POST', url: 'api/OrderInvoice/order-invoice', params: { invoiceId: params.invoiceId },
        }).then((res) => { if (res.status === 200) {   setInvoiceDetail(res.data[0]); } }).catch((err) => { })}
    /* useStates dan metode-metode untuk keperluan GET detail invoice */

    /* useStates untuk keperluan POST merk baru */
    const [imagePreview, setImagePreview] = useState("");
    const [base64, setBase64] = useState("");
    /* useStates untuk keperluan POST merk baru */

    /* Methods to convert image input into base64 */
    const onFileSubmit = (e) => {e.preventDefault();console.log(base64);}
    const onChange = (e) => {console.log("file", e.target.files[0]);let file = e.target.files[0];
        if (file) {const reader = new FileReader();reader.onload = _handleReaderLoaded;reader.readAsBinaryString(file) }}

    const _handleReaderLoaded = (readerEvt) => {let binaryString = readerEvt.target.result;setBase64(btoa(binaryString))}
    const photoUpload = (e) => {e.preventDefault();const reader = new FileReader();const file = e.target.files[0];console.log("reader", reader);console.log("file", file)
        if (reader !== undefined && file !== undefined) {reader.onloadend = () => {setImagePreview(reader.result)
            };reader.readAsDataURL(file);}}
    /* Methods to convert image input into base64 */

    /* Method to POST upload foto resi bukti pembayaran */
    const confirmPayment = async (e) => {
    e.preventDefault();
    const postDataa = {customer_id : invoiceDetail.customer_id, customer_email:invoiceDetail.customer_email, customer_address: invoiceDetail.customer_address, invoice_number: invoiceDetail.invoice_number, customer_phone: invoiceDetail.customer_phone, payment_method: invoiceDetail.payment_method, payment_total: invoiceDetail.payment_total, status: 'Terbayar', payment_verification_image: base64,saveType: "edit"};
    /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
    APIRequest.defaults.headers.common['Authorization'] = 'bearer ' + JSON.parse(localStorage.getItem('loggedInUserAuthToken'));
    /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
    await APIRequest({ method: 'POST',url: 'api/OrderInvoice/edit-order-invoice', params: { orderInvoiceId: params.invoiceId },data: postDataa,
    }).then((res) => {
        if (res.status === 200) {
            console.log(res.status)
            navigate(`/invoice-check/${invoiceDetail.id}`);
        }
    }).catch((err) => {console.log(err.response.data)})
    }
    /* Method to POST upload foto resi bukti pembayaran */

    /* Method to POST konfirmasi penerimaan pesanan */
    const confirmProductArrival =  (e) => {
        e.preventDefault();
        const postDataa = {customer_id : invoiceDetail.customer_id, customer_email:invoiceDetail.customer_email, invoice_number: invoiceDetail.invoice_number, customer_address: invoiceDetail.customer_address, invoice_number: invoiceDetail.invoice_number, customer_phone: invoiceDetail.customer_phone, payment_method: invoiceDetail.payment_method, payment_total: invoiceDetail.payment_total, status: 'Diterima', payment_verification_image: invoiceDetail.payment_verification_image,saveType: "edit"};
        /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
        APIRequest.defaults.headers.common['Authorization'] = 'bearer ' + JSON.parse(localStorage.getItem('loggedInUserAuthToken'));
        /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
         APIRequest({ method: 'POST',url: 'api/OrderInvoice/edit-order-invoice', params: { orderInvoiceId: params.invoiceId },data: postDataa,
        }).then((res) => {
            if (res.status === 200) {
                console.log(res.status)
                navigate(`/invoice-check/${invoiceDetail.id}`);
            }
        }).catch((err) => {console.log(err.response.data)})
        }
    /* Method to POST konfirmasi penerimaan pesanan */

    useEffect(() => { getInvoiceDetail(); }, [confirmPayment, confirmProductArrival])

    return (
        <div>

        <Card elevation={6} className="invoiceCheckCard" >

        <div style={{ textAlign: 'right'}}>
        <Button  component={Link} to="/invoice" variant="outlined"  color="info" > Kembali </Button>
        <Typography variant="overline" gutterBottom component="div">No. Invoice</Typography>
        <Typography variant="subtitle2" gutterBottom component="div">{invoiceDetail.invoice_number}</Typography>
        </div>

        { invoiceDetail.status === 'Checkout' ? 
        <div>

        <Button style={{ contentAlign: 'center', padding: '1%'}}  variant="contained" color="success">
        <Typography style={{ margin:0}} variant="h6" gutterBottom component="div">
        Checkout Berhasil!
        </Typography>
        </Button>

        <Alert style={{ margin: '2% 0' }} severity="warning">Anda telah berhasil checkout. Mohon melakukan pembayaran dan upload resi bukti pembayaran.</Alert>

        <Paper style={{ padding: '5%', marginBottom: '2%'}} elevation={6} >

        { invoiceDetail.payment_method === 'Bank BCA' ?
        <div>
        <Typography  variant="overline" gutterBottom component="div">Rekening BCA</Typography>
        <Typography  variant="h3" gutterBottom component="div"> 012 345 678</Typography>
        </div>
        : invoiceDetail.payment_method === 'Bank BRI' ?
        <div>
        <Typography  variant="overline" gutterBottom component="div">Rekening BRI</Typography>
        <Typography  variant="h3" gutterBottom component="div">  012 345 678</Typography>
        </div>
        : invoiceDetail.payment_method === 'Bank Mandiri' ?
        <div>
        <Typography  variant="overline" gutterBottom component="div">Rekening Mandiri</Typography>
        <Typography  variant="h3" gutterBottom component="div">  012 345 678</Typography>
        </div>
        : invoiceDetail.payment_method === 'GoPay' ?
        <div>
        <Typography  variant="overline" gutterBottom component="div">Rekening GoPay</Typography>
        <Typography  variant="h3" gutterBottom component="div">  012 345 678</Typography>
        </div>
        :
        <></>
    }
        
        <Typography  variant="h6" gutterBottom component="div">Upload Resi Pembayaran</Typography>
        <form style={{ margin: '0px auto' }} onSubmit={(e) => confirmPayment(e)} onChange={(e) => onChange(e)} >
        {imagePreview === "" ? "":<img style={{ height: '200px' }} src={imagePreview} alt="upload" />}
        <input type="file" name="avatar" id="file" accept=".jpef, .png, .jpg" onChange={photoUpload} src={imagePreview} />

        { base64 === "" ? 
        <div>
        
        <Button disabled style={{ marginTop: '2%' }} fullWidth color="primary" variant="contained">Konfirmasi Pembayaran</Button> 
        </div>:
        <Button type="submit" style={{ marginTop: '2%' }} fullWidth color="primary" variant="contained">Konfirmasi Pembayaran</Button>
        }

        </form>
        </Paper>
        </div>
        : invoiceDetail.status === 'Terbayar' ? 
        <div>

                    
        <Button style={{ contentAlign: 'center', padding: '1%'}}  variant="contained" color="success">
        <Typography style={{ margin:0}} variant="h6" gutterBottom component="div">
        Pembayaran Berhasil
        </Typography>
        </Button>

        <Alert style={{ margin: '2% 0' }} severity="primary">Anda telah berhasil mengupload bukti pembayaran. Mohon tunggu hingga admin toko mengkonfirmasi pembayaran</Alert>
        </div>
        : invoiceDetail.status === 'Terverifikasi' ? 
        <div>
                    
        <Button style={{ contentAlign: 'center', padding: '1%'}}  variant="contained" color="success">
        <Typography style={{ margin:0}} variant="h6" gutterBottom component="div">
        Pembayaran Terverifikasi
        </Typography>
        </Button>

        <Alert style={{ margin: '2% 0' }} severity="success">Admin telah memverifikasi pembayaran anda. Mohon tunggu foto resi pengiriman dari admin.</Alert>
        </div>
        : invoiceDetail.status === 'Dikirim' ? 
        <div>

        <Button style={{ contentAlign: 'center', padding: '1%'}}  variant="contained" color="success">
        <Typography style={{ margin:0}} variant="h6" gutterBottom component="div">
        Pesanan Telah Dikirimkan
        </Typography>
        </Button>

        <Alert style={{ margin: '2% 0' }} severity="warning">Admin telah mengupload resi pengiriman. Mohon konfirmasi pembayaran sesuai resi yang ditampilkan dibawah.</Alert>
        
        <Paper style={{ padding: '2%', marginBottom: '2%'}} elevation={6} >
        <form onSubmit={(e) => confirmProductArrival(e)} >
        <Button type="submit"  variant="contained" color="primary">Konfirmasi Penerimaan Pesanan</Button>
        </form>
        </Paper>
        </div>
        :
        <div>
                        
        <Button style={{ contentAlign: 'center', padding: '1%'}}  variant="contained" color="success">
        <Typography style={{ margin:0}} variant="h6" gutterBottom component="div">
        Pesanan Telah Diterima.
        </Typography>
        </Button>

        <Alert style={{ margin: '2% 0' }} severity="success">Pesanan ini sudah diterima.</Alert>
        </div>


    }

        <Grid container >
        <Grid item xs={12} md={4} >  
        <Typography  variant="overline" gutterBottom component="div">Total Pembayaran</Typography>
        <Typography  variant="subtitle1" gutterBottom component="div">{numberFormat(invoiceDetail.payment_total)}</Typography>
        </Grid>

        <Grid item xs={12} md={4} >  
        <Typography  variant="overline" gutterBottom component="div">Metode Pembayaran</Typography>
        <Typography  variant="subtitle1" gutterBottom component="div">{invoiceDetail.payment_method}</Typography>
        </Grid>

        <Grid item xs={12} md={4} >  
        <Typography  variant="overline" gutterBottom component="div">Alamat Penerima</Typography>
        <Typography  variant="subtitle2" gutterBottom component="div">{invoiceDetail.customer_address}</Typography>
        </Grid>

        <Grid item xs={12} md={4} >  
        <Typography  variant="overline" gutterBottom component="div">Email Pelanggan</Typography>
        <Typography  variant="subtitle2" gutterBottom component="div">{invoiceDetail.customer_email}</Typography>
        </Grid>

        <Grid item xs={12} md={4} >  
        <Typography  variant="overline" gutterBottom component="div">Telepon Pelanggan</Typography>
        <Typography  variant="subtitle2" gutterBottom component="div">{invoiceDetail.customer_phone}</Typography>
        </Grid>

        <Grid item xs={12} md={4} >  
        <Typography  variant="overline" gutterBottom component="div">Bukti Pembayaran -- Pengiriman</Typography>
        { invoiceDetail.payment_verification_image != "" ? <img style={{ height: '100px' }} src={`data:image/jpeg;base64,${invoiceDetail.payment_verification_image}`}  alt="Bukti Pembayaran" /> : <div>Belum Tersedia</div> }
        </Grid>
        </Grid>
      </Card>

  </div>
    );
  }