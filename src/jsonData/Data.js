let electronics = [
    {
      id: "1",
      name: "LG",
      image:"../assets/images/logoLG.jpg",
    },
    {
      id: "2",
      name: "Samsung",
      image:"../assets/images/logoSamsung.jpg",
    },
    {
      id: "3",
      name: "Panasonic",
      image:"../assets/images/logoPanasonic.jpg",
    },
    {
      id: "4",
      name: "Sharp",
      image:"../assets/images/logoSHarp.png",
    },
    {
      id: "5",
      name: "Fujitsu",
      image:"../assets/images/logoFujitsu.png",
    },
    {
      id: "6",
      name: "Hey",
      image:"../assets/images/logoSamsung.jpg",
    },
  ];

  let newArrivals = [
    {
      id: "1",
      name: "Speaker JT90",
      description: "merupakan sebuah Speaker JT90",
      brand: "Panasonic",
      brand_id: "3",
      price: 660800,
      sold: "12",
      rating: 4,
      image:"../assets/images/speakerJT90.jpg"
    },
    {
      id: "2",
      name: "Mesin Cuci 2 Tabung",
      description: "merupakan sebuah Mesin Cuci 2 Tabung",
      brand: "LG",
      brand_id: "1",
      price: 3860800,
      sold: "7",
      rating: 5,
      image:"../assets/images/mesinCuci2Tabung.jpg"
    },
    {
      id: "3",
      name: "Kulkas 2 Pintu",
      description: "merupakan sebuah Kulkas 2 Pintu",
      brand: "LG",
      brand_id: "1",
      price: 1460200,
      sold: "22",
      rating: 4,
      image:"../assets/images/kulkas2Pintu.jpg"
    },
    {
      id: "4",
      name: "TV Digital 30 Inch",
      description: "merupakan sebuah TV Digital 30 Inch",
      brand: "Sharp",
      brand_id: "4",
      price: 1200800,
      sold: "13",
      rating: 5,
      image:"../assets/images/tV30Inch.jpg"
    },
    {
      id: "5",
      name: "Speaker JT90",
      description: "merupakan sebuah Speaker JT90",
      brand: "Panasonic",
      brand_id: "3",
      price: 660800,
      sold: "12",
      rating: 4,
      image:"../assets/images/speakerJT90.jpg"
    },
    {
      id: "6",
      name: "Mesin Cuci 2 Tabung",
      description: "merupakan sebuah Mesin Cuci 2 Tabung",
      brand: "LG",
      brand_id: "1",
      price: 3860800,
      sold: "7",
      rating: 5,
      image:"../assets/images/mesinCuci2Tabung.jpg"
    },
  ];

  let bannerImages = [
    {
        name: "Random Name #1",
        description: "Probably the most random thing you have ever seen!",
        image: "./assets/images/banner1.jpg",
    },
    {
        name: "Random Name #2",
        description: "Hello World!",
        image: "./assets/images/banner2.jpg",
    },
    {
        name: "Random Name #2",
        description: "Hello World!",
        image: "./assets/images/banner2.jpg",
    },
    {
        name: "Random Name #2",
        description: "Hello World!",
        image: "./assets/images/banner2.jpg",
    },
];
  
  export function getElectronics() {
    return electronics;
  }
  export function getNewArrivals() {
    return newArrivals;
  }
  export function getBannerImages() {
    return bannerImages;
  }
