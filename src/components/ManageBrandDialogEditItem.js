import React, { useState, useEffect } from 'react';
import { 
    Box, Button, Dialog, TextField, Grid, DialogTitle, DialogContent
} from '@mui/material';
import { APIRequest } from './APICalls';

function ManageBrandDialogEditItem(props =  {
    open: false,
    id: props.id,
    onClose: () => { },
    onAdd: () => { }
}) {

        /* useStates untuk keperluan POST edit merk */
        const [brandName, setBrandName] = useState();
        const [brandDescription, setBrandDescription] = useState();
        const [imagePreview, setImagePreview] = useState("");
        const [base64, setBase64] = useState();
        useEffect(() => { 
            setBrandName(props.editItemData!= null ? props.editItemData.brand_name : "");
            setBrandDescription(props.editItemData!= null ? props.editItemData.description : "");
            setBase64(props.editItemData!= null ? props.editItemData.image : "");}, [props.editItemData])
        /* useStates untuk keperluan POST edit merk */

        /* Methods to convert image input into base64 */
        const onFileSubmit = (e) => {e.preventDefault();console.log(base64);}
        const onChange = (e) => {console.log("file", e.target.files[0]);let file = e.target.files[0];
            if (file) {const reader = new FileReader();reader.onload = _handleReaderLoaded;reader.readAsBinaryString(file) }}

        const _handleReaderLoaded = (readerEvt) => {let binaryString = readerEvt.target.result;setBase64(btoa(binaryString))}
        const photoUpload = (e) => {e.preventDefault();const reader = new FileReader();const file = e.target.files[0];console.log("reader", reader);console.log("file", file)
            if (reader !== undefined && file !== undefined) {reader.onloadend = () => {setImagePreview(reader.result)
              };reader.readAsDataURL(file);}}
        /* Methods to convert image input into base64 */

        /* Method to POST edit merk */
        const editBrand = async () => {
        const postDataa = {brand_name: brandName,description: brandDescription,image: base64,saveType: "edit"};
        /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
        APIRequest.defaults.headers.common['Authorization'] = 'bearer ' + JSON.parse(localStorage.getItem('loggedInUserAuthToken'));
        /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
        await APIRequest({ method: 'POST',url: 'api/Brand/edit-brand', params: { brandId: props.editItemData.id },data: postDataa,
        }).then((res) => {
            if (res.status === 200) {
                console.log(res.status)
                props.onClose()
            }
        }).catch((err) => {console.log(err.response.data)})
        }
        /* Method to POST edit merk */

    return (
        <div >
            <Dialog open={props.open} onClose={props.onClose} >
                <div style={{ padding: '20px', width: "100%" }}>
                    {/* TITLE */}
                    <DialogTitle>Edit Merk</DialogTitle>
                    <DialogContent >
                        {/* FORM INPUT */}

                        <form onSubmit={(e) => onFileSubmit(e)} onChange={(e) => onChange(e)} >
                                {imagePreview === "" ? "":<img style={{ width:'100%', height: '100%' }} src={imagePreview} alt="upload" />}
                                <input type="file" name="avatar" id="file" accept=".jpef, .png, .jpg" onChange={photoUpload} src={imagePreview} />
                        </form>

                        <form onSubmit={(e) => { e.preventDefault();editBrand()}} >
                            <Grid columnGap="10px" justifyContent="center" style={{ paddingBottom: "10px"}}>
                                <Grid >

                                    <Box noValidate>
                                    <TextField id="name"
                                        value={brandName}
                                        label="Nama Merk"
                                        onChange={(e) => setBrandName(e.target.value)}
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    />
                                    <TextField id="description"
                                        value={brandDescription}
                                        label="Deskripsi Merk"
                                        onChange={(e) => setBrandDescription(e.target.value)}
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    />

                                    <Button type='submit' fullWidth variant="contained"
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    >Terapkan Edit</Button>

                                    </Box>
                                </Grid>
                            </Grid>
                        </form>
                    </DialogContent> 
                </div>
            </Dialog>
        </div>
    )
}

export default ManageBrandDialogEditItem
