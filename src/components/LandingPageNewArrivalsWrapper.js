import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import { Link } from "react-router-dom";
import CardMedia from '@mui/material/CardMedia';
import Rating from '@mui/material/Rating';
import Badge from '@mui/material/Badge';
import SellIcon from '@mui/icons-material/Sell';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import numberFormat from './NumberFormat.js';
import { useState, useEffect } from 'react';
import { APIRequest } from './APICalls.js';
import { landingPageNewArrivals } from "../components/ReactMultiCarousel";
import { Box, CardActions, Grid } from '@mui/material';

export default function LandingPageNewArrivalsWrapper(props) {

    /* useStates dan metode-metode untuk keperluan GET daftar semua merk */
    const [refreshPage, setRefreshPage] = useState(false);
    const [searchQuery, setSearchQuery] = useState();
    const [listOfNewArrivals, setListOfBrands] = useState([]);
    const getListOfBrands = async () => {
        await APIRequest({
            method: 'GET', url: 'api/Product/list-all-products', params: { search: searchQuery },
        }).then((res) => { if (res.status === 200) { setListOfBrands(res.data); } }).catch((err) => { })
    }
    useEffect(() => { getListOfBrands(); }, [searchQuery, refreshPage])
    /* useStates untuk keperluan GET daftar semua merk */

    return (
        <div style={{ padding: '15px 0' }}>
            <Typography style={{ textTransform: 'capitalize', paddingBottom: "10px" }} >
                <h3 style={{ color: '#4F4F4F', margin: '0' }}>New Arrivals</h3>
            </Typography>

            <Carousel style={{ align: "center" }}
                swipeable={true}
                itemClass="carousel-item-padding-40-px"
                draggable={true}
                responsive={landingPageNewArrivals}
                autoPlay={true}
                infinite={true}
                autoPlaySpeed={3000}
                arrows={false}
                // removeArrowOnDeviceType={'mobile'}
            // centerMode={true}
            >
                {listOfNewArrivals.map((invoice) => (
                    <Grid item component={Link} to={`/detail/${invoice.id}`} >
                    <Card variant="outlined" style={{ border: "1px solid #BDBDBD", width: "250px", borderRadius: "10px", marginLeft: "auto", marginRight: "auto" }} >
                        <CardMedia style={{ objectFit: 'contain', paddingTop: "20px" }} component="img" height="100px" image={`data:image/jpeg;base64,${invoice.image}`} alt={invoice.image} />
                        <CardContent style={{ padding: "20px" }}>
                            <Badge style={{ opacity: '0.8' }} badgeContent={`Sold :${invoice.sold}`} color="primary">
                                <Typography component="div">
                                    <b>{invoice.name}</b>
                                </Typography>
                            </Badge>
                            <Typography variant="subtitle2" color="text.secondary">
                                {numberFormat(invoice.price)} 
                            </Typography>
                            <Typography variant="subtitle2" >
                                {invoice.description} <br />
                            </Typography>
                        </CardContent>
                        <CardActions style={{ padding: "0 20px 20px 20px", justifyContent: "center" }}>
                            <Button component={Link} to={`/detail/${invoice.id}`} style={{ width: '90%' }} className="newArrivalsCardAction" variant="contained" size="small">Detail</Button>
                        </CardActions>
                    </Card>
                    </Grid>
                ))}
            </Carousel>
        </div>
    )
}