import { 
    Box, Button, Dialog,  Grid, DialogTitle, DialogContent, TableCell, TableRow, TableHead, Table, TableBody, CardMedia
} from '@mui/material';
import { useEffect, useState } from 'react';
import { APIRequest } from '../components/APICalls';
import numberFormat from './NumberFormat';

function ManageInvoiceDialogViewItems(props =  {
    open: false,
    onClose: () => { },
    onAdd: () => { }
}) {

    /* useStates dan method untuk keperluan menampilkan daftar produk dari cart/invoice tertentu */
    const [invoiceId, setInvoiceId] = useState("");
    const [listOfProducts, setListOfProducts] = useState([]);
    useEffect(() => { 
        setInvoiceId(props.editItemData!= null ? props.editItemData.id  : "");}, [props.editItemData])
    const getListOfProductsByInvoiceId = async () => { await APIRequest({  method: 'GET', url: 'api/Product/list-products-by-invoice', params: { invoiceId: invoiceId },
    }).then((res) => { if (res.status === 200) {  setListOfProducts(res.data);console.log(listOfProducts); } }).catch((err) => { })}
    useEffect(() => { getListOfProductsByInvoiceId(); }, [invoiceId])
    /* useStates dan method untuk keperluan menampilkan daftar produk dari cart/invoice tertentu */

    return (
        <div >
            <Dialog open={props.open} onClose={props.onClose} >
                <div style={{ padding: '20px', width: "100%" }}>
                    {/* TITLE */}
                    <DialogTitle>Daftar Produk Yang Dipesan</DialogTitle>
                    <DialogContent >
                        {/* FORM INPUT */}

                        <form onSubmit={(e) => { e.preventDefault()}} >
                            <Grid columnGap="10px" justifyContent="center" style={{ paddingBottom: "10px"}}>
                                <Grid >

                                <Table  aria-label="simple table">
                                    <TableHead>
                                    <TableRow>
                                        <TableCell>Nama</TableCell>
                                        <TableCell align="right">Jumlah</TableCell>
                                        <TableCell align="right">Total Harga</TableCell>
                                        <TableCell align="right">Gambar</TableCell>
                                    </TableRow>
                                    </TableHead>
                                    <TableBody>
                                    {listOfProducts.map((row) => (
                                        <TableRow
                                        key={row.name}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                        <TableCell component="th" scope="row">
                                            {row.name}
                                        </TableCell>
                                        <TableCell align="right">{row.quantity}</TableCell>
                                        <TableCell align="right">{numberFormat(row.total_price) }</TableCell>
                                        <TableCell align="right"><CardMedia style={{ objectFit: 'contain'}} component="img" height="160" image={`data:image/jpeg;base64,${row.image}`} alt={row.name} /></TableCell>
                                        </TableRow>
                                    ))}
                                    </TableBody>
                                </Table>

                                </Grid>
                            </Grid>
                        </form>
                    </DialogContent> 
                </div>
            </Dialog>
        </div>
    )
}

export default ManageInvoiceDialogViewItems
