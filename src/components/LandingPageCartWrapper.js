import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { Button, Grid } from '@mui/material';
import { Link } from "react-router-dom";

export default function LandingPageCartWrapper(props) {
    return (
        <Grid container spacing={1}>
            <Grid item xs={12} md={9} />
            <Grid item xs={12} md={3}>
                <div className="cartWrapper" style={{ padding: '20px 40px 5px 40px' }}>
                    <Button style={{ textTransform: 'capitalize' }}>
                        <Link to="/cart" >Cek Keranjang Belanjamu</Link>
                        <ShoppingCartIcon style={{ paddingLeft: '10px', color: '#FF7596' }} />
                    </Button>
                </div>
            </Grid>
        </Grid>
    )
}