import * as React from 'react';
import Divider from '@mui/material/Divider';
import Paper from '@mui/material/Paper';
import MenuList from '@mui/material/MenuList';
import MenuItem from '@mui/material/MenuItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import HomeIcon from '@mui/icons-material/Home';
import ListIcon from '@mui/icons-material/List';
import CategoryIcon from '@mui/icons-material/Category';
import LogoutIcon from '@mui/icons-material/Logout';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import { Link } from 'react-router-dom';

export default function AdminSidebarMenu() {
  return (
    <Paper >
      <MenuList>
        <MenuItem component={Link} to={'/'} >
          <ListItemIcon>
            <HomeIcon fontSize="small" />
          </ListItemIcon>
          <Link style={{ color: 'black'}} to={'/'} >Beranda</Link>
        </MenuItem>
        <Divider />
        <MenuItem component={Link} to={'/admin/products'}>
          <ListItemIcon>
            <ListIcon fontSize="small" />
          </ListItemIcon>
          <Link style={{ color: 'black'}} to={'/admin/products'} >Kelola Katalog</Link>
        </MenuItem>
        <MenuItem component={Link} to={'/admin/brands'}>
          <ListItemIcon>
            <CategoryIcon fontSize="small" />
          </ListItemIcon>
          <Link style={{ color: 'black'}} to={'/admin/brands'} >Kelola Merk</Link>
        </MenuItem>
        <MenuItem component={Link} to={'/admin/invoices'}>
          <ListItemIcon>
            <LocalShippingIcon fontSize="small" />
          </ListItemIcon>
          <Link  style={{ color: 'black'}} to={'/admin/invoices'} >Kelola Pesanan</Link>
        </MenuItem> 
        <MenuItem component={Link} to={'/'}>
          <ListItemIcon>
            <LogoutIcon fontSize="small" />
          </ListItemIcon>
          <Link  style={{ color: 'black'}} to={'/'} >Log Out</Link>
        </MenuItem> 
      </MenuList>
    </Paper>
  );
}