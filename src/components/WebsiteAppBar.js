import * as React from 'react';
import { Link, useNavigate, createSearchParams } from 'react-router-dom';
import {
    AppBar, Box, Toolbar, Typography, Button, styled, InputBase, Tooltip, Zoom, Drawer
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import AssignmentIcon from '@mui/icons-material/Assignment';
import LogoutIcon from '@mui/icons-material/Logout';
import MenuIcon from '@mui/icons-material/Menu';

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: 'white',
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
    },
    flexGrow: 1
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit', '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '40ch',
        },
    },
}));

function HeaderBar() {

    const [searchQuery, setSearchQuery] = React.useState("")
    let navigate = useNavigate();
    const handleSearch = () => {
        navigate({
            pathname: "/search",
            search: createSearchParams({
                search: searchQuery
            }).toString()
        });
    }

    const [openSearchDrawer, setOpenSearchDrawer] = React.useState(false);
    const [openMenuDrawer, setOpenMenuDrawer] = React.useState(false);

    const toggleSearchDrawer = (anchor, open) => (event) => {
        setOpenSearchDrawer(false);
      };
    const toggleMenuDrawer = (anchor, open) => (event) => {
        setOpenMenuDrawer(false);
    };

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar variant="dense" position="fixed" style={{ backgroundColor: '#FF7596' }}>
                <Toolbar>
                    <Link to="/">
                        <Typography variant="h6" component="div"
                            style={{ color: 'white' }}
                        > 1 Electronic
                        </Typography>
                    </Link>

                    <div className='appBarSearchForm' >
                    <form onSubmit={(e) => { e.preventDefault(); handleSearch() }} >
                        <Search>
                            <SearchIconWrapper>
                                <SearchIcon style={{ color: '#FF7596' }} />
                            </SearchIconWrapper>
                            <StyledInputBase
                                value={searchQuery}
                                onChange={(e) => setSearchQuery(e.target.value)}
                                placeholder="Cari elektronik"
                                inputProps={{ 'aria-label': 'search' }}
                                style={{ color: '#000000' }}
                            />
                        </Search>
                    </form>
                    </div>

                    
                    <Box sx={{ flexGrow: 1 }} />
                    <div className='desktopAppBarRightsideIcons' >
                    {JSON.parse(localStorage.getItem('loggedInUserRole')) === 'admin' ?

                        <div>
                            <Button component={Link} to="/admin/products" variant="text" color="inherit"
                                style={{
                                    textTransform: 'capitalize',
                                    paddingRight: '10px'
                                }}
                            >Dashboard</Button>

                            <Tooltip TransitionComponent={Zoom} title="Logout">
                                <Button variant="text" color="inherit"
                                    onClick={() => {
                                        localStorage.removeItem("loggedInUserAuthToken");
                                        localStorage.removeItem("loggedInUserId");
                                        localStorage.removeItem("loggedInUserRole");
                                        console.log('Local Storage Removed'); navigate("/")
                                    }}
                                > <LogoutIcon />
                                </Button>
                            </Tooltip>
                        </div>
                        :
                        JSON.parse(localStorage.getItem('loggedInUserRole')) === 'buyer' ?
                            <div>
                                <Tooltip TransitionComponent={Zoom} title="Invoice">
                                    <Button component={Link} to="/invoice" variant="text" color="inherit"
                                        style={{
                                            // textTransform: 'capitalize',
                                            marginRight: '10px'
                                        }}
                                    ><AssignmentIcon /></Button>
                                </Tooltip>

                                <Tooltip TransitionComponent={Zoom} title="Logout">
                                    <Button variant="text" color="inherit"
                                        onClick={() => {
                                            localStorage.removeItem("pendingCartId");
                                            localStorage.removeItem("loggedInUserAuthToken");
                                            localStorage.removeItem("loggedInUserId"); localStorage.removeItem("loggedInUserRole");
                                            console.log('Local Storage Removed'); navigate("/")
                                        }}
                                    > <LogoutIcon />
                                    </Button>
                                </Tooltip>
                            </div>
                            :
                            <div>
                                <Button component={Link} to="/register" variant="text" color="inherit"
                                    style={{
                                        textTransform: 'capitalize',
                                        marginRight: '20px'
                                    }}
                                >Daftar Sekarang</Button>

                                <Button component={Link} to="/login" variant="contained" color="inherit"
                                    style={{ color: '#FF7596', textTransform: 'capitalize', backgroundColor: 'white', }}
                                > Masuk
                                </Button>
                            </div>
                    }
                </div>

                <div className="mobileAppBarRightsideIcons" >

                <Button onClick={() => {setOpenSearchDrawer(true)}}><SearchIcon style={{ color: 'white'}} /></Button>
                <Button onClick={() => {setOpenMenuDrawer(true)}} ><MenuIcon style={{ color: 'white'}} /></Button>

                <Drawer anchor={'top'} open={openSearchDrawer} onClose={toggleSearchDrawer('top', false)}  >
                <div className="mobileAppBarMenuDrawer">
                
                    <div style={{ padding: '5%'}} >
                    <form onSubmit={(e) => { e.preventDefault(); toggleSearchDrawer('top',false);handleSearch() ;}} >
                        <Search>
                            <SearchIconWrapper>
                                <SearchIcon style={{ color: '#FF7596' }} />
                            </SearchIconWrapper>
                            <StyledInputBase
                                value={searchQuery}
                                onChange={(e) => setSearchQuery(e.target.value)}
                                placeholder="Cari elektronik"
                                inputProps={{ 'aria-label': 'search' }}
                                style={{ color: '#000000' }}
                            />
                        </Search>
                    </form>
                    </div>
                </div>
                </Drawer>


                <Drawer anchor={'top'} open={openMenuDrawer} onClose={toggleMenuDrawer('top', false)}  >
                <div className="mobileAppBarMenuDrawer">

                        {JSON.parse(localStorage.getItem('loggedInUserRole')) === 'admin' ?

                        <div style={{ padding: '5%'}}>
                            <Button fullWidth component={Link} to="/admin/products" variant="text"
                                style={{
                                    textTransform: 'capitalize',
                                    paddingRight: '10px',
                                    color: "white"
                                }}
                            >Dashboard</Button>

                            <Tooltip TransitionComponent={Zoom} title="Logout">
                                <Button fullWidth variant="text" 
                                    onClick={() => {
                                        localStorage.removeItem("loggedInUserAuthToken");
                                        localStorage.removeItem("loggedInUserId");
                                        localStorage.removeItem("loggedInUserRole");
                                        console.log('Local Storage Removed'); navigate("/")
                                    }}
                                > <LogoutIcon style={{ color: "white" }} />
                                </Button>
                            </Tooltip>
                        </div>
                        :
                        JSON.parse(localStorage.getItem('loggedInUserRole')) === 'buyer' ?
                            <div style={{ padding: '5%'}}>
                                <Tooltip TransitionComponent={Zoom} title="Invoice">
                                    <Button fullWidth component={Link} to="/invoice" variant="text" 
                                        style={{
                                            // textTransform: 'capitalize',
                                            marginRight: '10px',
                                            color: "white"
                                        }}
                                    ><AssignmentIcon /></Button>
                                </Tooltip>

                                <Tooltip TransitionComponent={Zoom} title="Logout">
                                    <Button fullWidth variant="text" 
                                        onClick={() => {
                                            localStorage.removeItem("pendingCartId");
                                            localStorage.removeItem("loggedInUserAuthToken");
                                            localStorage.removeItem("loggedInUserId"); localStorage.removeItem("loggedInUserRole");
                                            console.log('Local Storage Removed'); navigate("/")
                                        }}
                                    > <LogoutIcon style={{ color: "white" }} />
                                    </Button>
                                </Tooltip>
                            </div>
                            :
                            <div style={{ padding: '5%'}}>
                                <Button fullWidth component={Link} to="/register" variant="outlined" color="inherit"
                                    style={{
                                        textTransform: 'capitalize',
                                        color:'white'
                                    }}
                                >Daftar Sekarang</Button>

                                <br/><br/>

                                <Button fullWidth component={Link} to="/login" variant="contained" color="inherit"
                                    style={{ color: '#FF7596',textTransform: 'capitalize', backgroundColor: 'white', }}
                                > Masuk
                                </Button>
                            </div>
                    }
                    
                </div></Drawer>

                </div>
                </Toolbar>
            </AppBar>
        </Box>
    );
}

export default HeaderBar;
