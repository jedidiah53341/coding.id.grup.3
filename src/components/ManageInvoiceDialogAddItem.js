import React, { useState } from 'react';
import { 
    Box, Button, Dialog, TextField, Grid, DialogTitle, DialogContent, MenuItem, Select, InputLabel
} from '@mui/material';
import { APIRequest } from './APICalls';

function ManageInvoiceDialogAddItem(props =  {
    open: false,
    id: 0,
    onClose: () => { },
    onAdd: () => { }
}) {

        /* useStates untuk keperluan POST merk baru */
        const [customerId, setCustomerId] = useState();
        const [customerEmail, setCustomerEmail] = useState();
        const [customerPhone, setCustomerPhone] = useState();
        const [paymentMethod, setPaymentMethod] = useState();
        const [paymentTotal, setPaymentTotal] = useState();
        const [status, setStatus] = useState();
        const [imagePreview, setImagePreview] = useState("");
        const [base64, setBase64] = useState();
        /* useStates untuk keperluan POST merk baru */

        /* Methods to convert image input into base64 */
        const onFileSubmit = (e) => {e.preventDefault();console.log(base64);}
        const onChange = (e) => {console.log("file", e.target.files[0]);let file = e.target.files[0];
            if (file) {const reader = new FileReader();reader.onload = _handleReaderLoaded;reader.readAsBinaryString(file) }}

        const _handleReaderLoaded = (readerEvt) => {let binaryString = readerEvt.target.result;setBase64(btoa(binaryString))}
        const photoUpload = (e) => {e.preventDefault();const reader = new FileReader();const file = e.target.files[0];console.log("reader", reader);console.log("file", file)
            if (reader !== undefined && file !== undefined) {reader.onloadend = () => {setImagePreview(reader.result)
              };reader.readAsDataURL(file);}}
        /* Methods to convert image input into base64 */

        /* Method to POST new Brand Item */
        const postBrand = () => {
        const postDataa = {customer_id : customerId, customer_email:customerEmail, customer_phone: customerPhone, payment_method: paymentMethod, payment_total: paymentTotal, status: status, payment_verification_image: base64,saveType: "add"};
        /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
        APIRequest.defaults.headers.common['Authorization'] = 'bearer ' + JSON.parse(localStorage.getItem('loggedInUserAuthToken'));
        /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
        APIRequest({ method: 'POST',url: 'api/OrderInvoice/post-order-invoice',data: postDataa,
        }).then((res) => {
            if (res.status === 200) {
                props.onClose()
            }
        }).catch((err) => {console.log(err.response.data)})
        }
        /* Method to POST new Brand Item */

    return (
        <div >
            <Dialog open={props.open} onClose={props.onClose} >
                <div style={{ padding: '20px', width: "100%" }}>
                    {/* TITLE */}
                    <DialogTitle>Tambahkan Invoice Baru</DialogTitle>
                    <DialogContent >
                        {/* FORM INPUT */}
                        <form onSubmit={(e) => onFileSubmit(e)} onChange={(e) => onChange(e)} >
                                {imagePreview === "" ? "":<img style={{ width:'100%', height: '100%' }} src={imagePreview} alt="upload" />}
                                <input type="file" name="avatar" id="file" accept=".jpef, .png, .jpg" onChange={photoUpload} src={imagePreview} />
                        </form>

                        <form onSubmit={(e) => { e.preventDefault();postBrand()}} >
                            <Grid columnGap="10px" justifyContent="center" style={{ paddingBottom: "10px"}}>
                                <Grid >

                                    <Box noValidate>
                                    <TextField 
                                        value={customerId }
                                        label="id Pelanggan"
                                        onChange={(e) => setCustomerId(e.target.value)}
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    />
                                    <TextField 
                                        value={customerEmail }
                                        label="Email Pelanggan"
                                        onChange={(e) => setCustomerEmail(e.target.value)}
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    />
                                    <TextField 
                                        value={customerPhone }
                                        label="No. Telepon Pelanggan"
                                        onChange={(e) => setCustomerPhone(e.target.value)}
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    />
                                     <TextField 
                                        value={paymentMethod }
                                        label="Metode Pembayaran"
                                        onChange={(e) => setPaymentMethod(e.target.value)}
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    />
                                     <TextField 
                                        value={paymentTotal }
                                        label="Total Pembayaran"
                                        onChange={(e) => setPaymentTotal(e.target.value)}
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    />
                                    <InputLabel>Status Invoice</InputLabel>
                                    <Select
                                        fullWidth
                                        value={status}
                                        label="Status Invoice"
                                        onChange={(e) => setStatus(e.target.value)}
                                    >
                                        <MenuItem value="Pending">Pending</MenuItem>
                                        <MenuItem value="Checkout">Checkout</MenuItem>
                                        <MenuItem value="Terbayar">Terbayar</MenuItem>
                                        <MenuItem value="Terverifikasi">Terverifikasi</MenuItem>
                                        <MenuItem value="Dikirim">Dikirim</MenuItem>
                                        <MenuItem value="Diterima">Diterima</MenuItem>
                                    </Select>  

                                    <Button type='submit' fullWidth variant="contained"
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    >Tambahkan Invoice Baru</Button>
                                    </Box>
                                </Grid>
                            </Grid>
                        </form>
                    </DialogContent> 
                </div>
            </Dialog>
        </div>
    )
}

export default ManageInvoiceDialogAddItem
