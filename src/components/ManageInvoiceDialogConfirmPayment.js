import React, { useState, useEffect } from 'react';
import { 
    Box, Button, Dialog, TextField, Grid, DialogTitle, DialogContent
} from '@mui/material';
import { APIRequest } from './APICalls';

function ManageInvoiceDialogConfirmPayment(props =  {
    open: false,
    id: props.id,
    onClose: () => { },
    onAdd: () => { }
}) {

        /* useStates untuk keperluan POST merk baru */
        const [customerId, setCustomerId] = useState();
        const [customerEmail, setCustomerEmail] = useState();
        const [customerPhone, setCustomerPhone] = useState();
        const [paymentMethod, setPaymentMethod] = useState();
        const [paymentTotal, setPaymentTotal] = useState();
        const [invoiceNumber, setInvoiceNuber] = useState();
        const [customerAddress, setCustomerAddress] = useState();
        const [status, setStatus] = useState();
        const [imagePreview, setImagePreview] = useState("");
        const [base64, setBase64] = useState();
        /* useStates untuk keperluan POST merk baru */
        useEffect(() => { 
            setCustomerId(props.editItemData!= null ? props.editItemData.customer_id  : "");
            setCustomerEmail(props.editItemData!= null ? props.editItemData.customer_email : "");
            setCustomerPhone(props.editItemData!= null ? props.editItemData.customer_phone : "");
            setPaymentMethod(props.editItemData!= null ? props.editItemData.payment_method : "");
            setInvoiceNuber(props.editItemData!= null ? props.editItemData.invoice_number : "");
            setCustomerAddress(props.editItemData!= null ? props.editItemData.customer_address : "");
            setPaymentTotal(props.editItemData!= null ? props.editItemData.payment_total : "");
            setStatus(props.editItemData!= null ? props.editItemData.status : "");
            setBase64(props.editItemData!= null ? props.editItemData.payment_verification_image : "");}, [props.editItemData])
        /* useStates untuk keperluan POST edit merk */

        /* Methods to convert image input into base64 */
        const onFileSubmit = (e) => {e.preventDefault();console.log(base64);}
        const onChange = (e) => {console.log("file", e.target.files[0]);let file = e.target.files[0];
            if (file) {const reader = new FileReader();reader.onload = _handleReaderLoaded;reader.readAsBinaryString(file) }}

        const _handleReaderLoaded = (readerEvt) => {let binaryString = readerEvt.target.result;setBase64(btoa(binaryString))}
        const photoUpload = (e) => {e.preventDefault();const reader = new FileReader();const file = e.target.files[0];console.log("reader", reader);console.log("file", file)
            if (reader !== undefined && file !== undefined) {reader.onloadend = () => {setImagePreview(reader.result)
              };reader.readAsDataURL(file);}}
        /* Methods to convert image input into base64 */

        /* Method to POST edit merk */
        const editBrand = async () => {
        const postDataa = {customer_id : customerId, customer_email:customerEmail, customer_address: customerAddress, invoice_number: invoiceNumber, customer_phone: customerPhone, payment_method: paymentMethod, payment_total: paymentTotal, status: "Terverifikasi", payment_verification_image: base64,saveType: "edit"};
        /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
        APIRequest.defaults.headers.common['Authorization'] = 'bearer ' + JSON.parse(localStorage.getItem('loggedInUserAuthToken'));
        /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
        await APIRequest({ method: 'POST',url: 'api/OrderInvoice/edit-order-invoice', params: { orderInvoiceId: props.editItemData.id },data: postDataa,
        }).then((res) => {
            if (res.status === 200) {
                console.log(res.status)
                props.onClose()
            }
        }).catch((err) => {console.log(err.response.data)})
        }
        /* Method to POST edit merk */

    return (
        <div >
            <Dialog open={props.open} onClose={props.onClose} >
                <div style={{ padding: '20px', width: "100%" }}>
                    {/* TITLE */}
                    <DialogTitle>Konfirmasi Pembayaran Invoice</DialogTitle>
                    <DialogContent >
                        {/* FORM INPUT */}

                        <form onSubmit={(e) => { e.preventDefault();editBrand()}} >
                            <Grid columnGap="10px" justifyContent="center" style={{ paddingBottom: "10px"}}>
                                <Grid >

                                    <Button type='submit' fullWidth variant="contained"
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    >Alright fam</Button>

                                </Grid>
                            </Grid>
                        </form>
                    </DialogContent> 
                </div>
            </Dialog>
        </div>
    )
}

export default ManageInvoiceDialogConfirmPayment
