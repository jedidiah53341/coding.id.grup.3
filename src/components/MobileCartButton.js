import IconButton from '@mui/material/IconButton';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { Link } from 'react-router-dom';

export default function MobileCartButton(props) {
    return (
        <div className="mobileCartButtonWrapper">
            <IconButton component={Link} to="/cart"  variant="contained" size="large">
                <ShoppingCartIcon style={{ color: 'white' }} fontSize="25px" />
            </IconButton>
        </div>
    )
}