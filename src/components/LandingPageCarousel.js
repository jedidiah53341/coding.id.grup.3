import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import CardMedia from '@mui/material/CardMedia';
import { landingPageResponsive } from "../components/ReactMultiCarousel";

import { getBannerImages } from "../jsonData/Data.js";
let items = getBannerImages();
export default function LandingPageCarousel(props) {
    return (
        <div>
        <div className="desktopLandingPageCarousel" style={{ padding: '15px 0' }}>
            <Carousel
                centerMode={true}
                swipeable={true}
                arrows={false}
                itemClass="carousel-item-padding-40-px"
                draggable={true}
                responsive={landingPageResponsive}
                autoPlay={true}
                autoPlaySpeed={3000}
                infinite={true}
                // transitionDuration={2000}
            >
                {
                    items.map((item, i) => <CardMedia className="carouselImage" component="img" image={item.image} alt="green iguana" />)
                }
            </Carousel>
        </div>

        <div className="mobileLandingPageCarousel" style={{ padding: '15px 0' }}>
            <Carousel
                swipeable={true}
                arrows={false}
                itemClass="carousel-item-padding-40-px"
                draggable={true}
                responsive={landingPageResponsive}
                autoPlay={true}
                autoPlaySpeed={3000}
                infinite={true}
                transitionDuration={2000}
            >
                {
                    items.map((item, i) => <CardMedia className="carouselImage" component="img" image={item.image} alt="green iguana" />)
                }
            </Carousel>
        </div>
        </div>
    )
}