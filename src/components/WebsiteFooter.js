import * as React from 'react';
import {
    Box, Grid, Typography, styled, Button
} from '@mui/material';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import TelegramIcon from '@mui/icons-material/Telegram';
import MailOutlineTwoToneIcon from '@mui/icons-material/MailOutlineTwoTone';
// import { getElectronics } from '../jsonData/Data';
import { Link } from "react-router-dom";
import { useState, useEffect } from 'react';
import { APIRequest } from './APICalls';

const BoxIcon = styled('div')(({ theme }) => ({
    position: 'relative',
    width: '40px',
    height: '40px',
    backgroundColor: 'white',
    borderRadius: '50%',
    display: 'table-cell',
    verticalAlign: 'middle',
    textAlign: 'center'
}))


function Footer() {

    /* useStates dan metode-metode untuk keperluan GET daftar semua merk */
    const [listOfBrands, setListOfBrands] = useState([]);
    const getListOfBrands = async () => {
        await APIRequest({
            method: 'GET', url: 'api/Brand/list-all-brands', params: { search: "" },
        }).then((res) => { if (res.status === 200) { setListOfBrands(res.data); } }).catch((err) => { })
    }
    useEffect(() => { getListOfBrands(); }, [])
    /* useStates untuk keperluan GET daftar semua merk */

    return (
        <div className="footerWrapper" >
            <Box sx={{ widht: '100%', height: 'auto', }} >

                <Grid container spacing={3}
                    style={{ padding: '20px 0' }}
                >
                    <Grid item xs={12} md={4} style={{ color: 'white' }}>
                        <Typography variant="h6" component="div" >
                            1Electronic
                        </Typography>
                        <Typography variant="body2">
                            Kami adalah toko elektronik online pertama
                            dan terbesar sejagat raya, jadi ayok buruan di
                            serbu semua peralatan elektronik di toko Kami
                            <br /><br />
                            Promo menarik selalu ada setiap Sabtu dan
                            Minggu !!
                        </Typography>

                    </Grid>

                    <Grid item xs={12} md={4} style={{ color: 'white' }}>
                        <Typography variant="h6" component="div" >
                            Merek
                        </Typography>

                        <Grid container columnGap="0px"  >
                            {listOfBrands.map((invoice) => (
                                <Grid item xs={6} md={4}>
                                    <Link style={{ color: 'white' }} to={`/category/${invoice.id}`} >
                                        <Typography variant="body2">{invoice.brand_name}</Typography>
                                    </Link>
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>

                    <Grid item xs={12} md={4} style={{ color: 'white' }}>
                        <Typography variant="h6" component="div" >
                            Kontak <br />
                        </Typography>
                        <Typography variant="body2">
                            Jl.Suka Maju Blok O No 28, Billford, Rock <br />
                            Island, Newland <br /><br />
                            021-12345678 <br />
                            <Grid container columnGap="10px" style={{ color: '#FF7596', padding: '10px 0' }} >
                                <Grid>
                                    <BoxIcon>
                                        <a target="_blank" rel="noreferrer" style={{ color: '#FF7596' }} href="http://instagram.com/"><InstagramIcon style={{ fontSize: '25px', paddingTop: '3px' }} /></a>
                                    </BoxIcon>
                                </Grid>
                                <Grid>
                                    <BoxIcon>
                                        <a target="_blank" rel="noreferrer" style={{ color: '#FF7596' }} href="http://youtube.com/"><YouTubeIcon style={{ fontSize: '25px', paddingTop: '3px' }} /></a>
                                    </BoxIcon>
                                </Grid>
                                <Grid>
                                    <BoxIcon>
                                        <a target="_blank" rel="noreferrer" style={{ color: '#FF7596' }} href="http://telegram.com/"><TelegramIcon style={{ fontSize: '25px', paddingTop: '3px' }} /></a>
                                    </BoxIcon>
                                </Grid>
                                <Grid>
                                    <BoxIcon>
                                        <a target="_blank" rel="noreferrer" style={{ color: '#FF7596' }} href="http://mail.com/"><MailOutlineTwoToneIcon style={{ fontSize: '25px', paddingTop: '3px' }} /></a>
                                    </BoxIcon>
                                </Grid>
                            </Grid>
                        </Typography>
                    </Grid>
                </Grid>
            </Box>
        </div>
    )
}

export default Footer;