import React, { useState } from 'react';
import { 
    Box, Button, Dialog, TextField, Grid, DialogTitle, DialogContent,
    InputLabel, Select, MenuItem, FormControl
} from '@mui/material';
import { APIRequest } from './APICalls';
import HideImageIcon from '@mui/icons-material/HideImage';

function ManageProductDialogAddItem(props =  {
    open: false,
    id: 0,
    onClose: () => { },
    onAdd: () => { }
}) {
    const handleChange = (event) => {
        setProductBrandId(event.target.value);
    };

    /* useStates untuk keperluan POST merk baru */
    const [productName, setProductName] = useState();
    const [productDescription, setProductDescription] = useState();
    const [productBrandId, setProductBrandId] = useState();
    const [productPrice, setProductPrice] = useState();
    const [productSold, setProductSold] = useState();
    const [productRating, setProductRating] = useState(3);
    const [imagePreview, setImagePreview] = useState("");
    const [base64, setBase64] = useState();
    /* useStates untuk keperluan POST merk baru */

    /* Methods to convert image input into base64 */
    const onFileSubmit = (e) => {
        e.preventDefault();
        console.log(base64);
    }
    const onChange = (e) => {
        console.log("file", e.target.files[0]);
        let file = e.target.files[0];
        if (file) {const reader = new FileReader();
            reader.onload = _handleReaderLoaded;
            reader.readAsBinaryString(file) 
        }
    }

    const _handleReaderLoaded = (readerEvt) => {
        let binaryString = readerEvt.target.result;
        setBase64(btoa(binaryString))
    }
    const photoUpload = (e) => {
        e.preventDefault();
        const reader = new FileReader();
        const file = e.target.files[0];
        console.log("reader", reader);
        console.log("file", file)
        if (reader !== undefined && file !== undefined) {
            reader.onloadend = () => {
                setImagePreview(reader.result)
          }; 
          reader.readAsDataURL(file);
        }
    }
    /* Methods to convert image input into base64 */

    /* Method to POST new Product Item */
    const postProduct = () => {
        const postDataa = { 
            name: productName, 
            description: productDescription,
            brand_id: productBrandId,
            price: productPrice,
            sold: productSold,
            rating: productRating,
            image: base64,
            saveType: "add"
        };
        /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
        APIRequest.defaults.headers.common['Authorization'] = 'bearer ' + JSON.parse(localStorage.getItem('loggedInUserAuthToken'));
        /* Gunakan line ini untuk menambahkan Authorization header ke APIRequestnya */
        APIRequest({ 
            method: 'POST',
            url: 'api/Product/post-product',
            data: postDataa,
        }).then((res) => {
            if (res.status === 200) { props.onClose() }
        }).catch((err) => { console.log(err.response.data) })
    }
    /* Method to POST new Product Item */

    return (
        <div >
            <Dialog open={props.open} onClose={props.onClose} maxWidth="1000px">
                <Box style={{ padding: '20px', width: "800" }}>
                    {/* TITLE */}
                    <DialogTitle>Add Item</DialogTitle>
                    <DialogContent >
                        {/* FORM INPUT */}
                        <Grid container columnGap="30px" justifyContent="center" style={{ paddingBottom: "" }}>
                            <Grid style={{  }} >
                                <Box style={{ 
                                        height: '100%', 
                                        width: '300px', 
                                        display: 'flex', 
                                        justifyContent: 'center', 
                                        alignItems: 'center',
                                        // border: "2px solid #FF7596", borderRadius: "10px"
                                    }}
                                >
                                    { imagePreview
                                        ? <img src={imagePreview} alt="upload" width="250px" height="250px" />
                                        : <HideImageIcon sx={{ height: '100%', width: '300px', }} />
                                    }
                                </Box>
                            </Grid>

                            <Grid >
                                <form onSubmit={(e) => onFileSubmit(e)} onChange={(e) => onChange(e)}>
                                    <input type="file" name="avatar" id="file" accept=".jpef, .png, .jpg" 
                                        style={{ paddingBottom: "10px"}} 
                                        onChange={ photoUpload } src={imagePreview}>
                                    </input>
                                </form>
                                <form onSubmit={(e) => { e.preventDefault(); postProduct() }} >
                                    <Box noValidate>
                                    <TextField id="name"
                                        value={productName}
                                        label="Nama Item"
                                        onChange={(e) => setProductName(e.target.value)}
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    />
                                    <TextField id="description"
                                        value={productDescription}
                                        label="Deskripsi Item"
                                        multiline
                                        rows={3}
                                        onChange={(e) => setProductDescription(e.target.value)}
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    />
                                    <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Brand</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"                          
                                        value={productBrandId}
                                        label="Brand"
                                        onChange={handleChange}
                                        style={{ display: 'flex', flexGrow: 1,}}
                                    >
                                        <MenuItem value="1">LG</MenuItem>
                                        <MenuItem value="2">Samsung</MenuItem>
                                        <MenuItem value="3">Panasonic</MenuItem>
                                        <MenuItem value="4">Sharp</MenuItem>
                                        <MenuItem value="5">Fujuitsu</MenuItem>
                                    </Select>
                                    </FormControl>
                                    <TextField id="price"
                                        value={productPrice}
                                        label="Price"
                                        onChange={(e) => setProductPrice(e.target.value)}
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    />
                                    <TextField id="sold"
                                        value={productSold}
                                        label="Jumlah Item Terjual"
                                        rows={3}
                                        onChange={(e) => setProductSold(e.target.value)}
                                        style={{ display: 'flex', flexGrow: 1, marginTop: '20px', marginBottom: '20px' }}
                                    />
                                    <Button type="submit" variant="contained" fullWidth style={{color:"white"}} >
                                        Tambahkan Product Baru
                                    </Button>
                                    </Box>
                                </form>
                            </Grid>
                        </Grid>
                    </DialogContent> 
                </Box>
            </Dialog>
        </div>
    )
}

export default ManageProductDialogAddItem
