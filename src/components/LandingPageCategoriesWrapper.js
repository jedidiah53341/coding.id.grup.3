import Typography from '@mui/material/Typography';
import { getElectronics } from "../jsonData/Data.js";
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { Link } from "react-router-dom";
import { useState, useEffect } from 'react';
import { APIRequest } from './APICalls.js';
import { landingPageCategoriesResponsive } from "../components/ReactMultiCarousel";
import { borderRadius, Box } from '@mui/system';

export default function LandingPageCategoriesWrapper(props) {

    /* useStates dan metode-metode untuk keperluan GET daftar semua merk */
    const [refreshPage, setRefreshPage] = useState(false);
    const [searchQuery, setSearchQuery] = useState();
    const [listOfBrands, setListOfBrands] = useState([]);
    const getListOfBrands = async () => {
        await APIRequest({
            method: 'GET', url: 'api/Brand/list-all-brands', params: { search: searchQuery },
        }).then((res) => { if (res.status === 200) { setListOfBrands(res.data); } }).catch((err) => { })
    }
    useEffect(() => { getListOfBrands(); }, [searchQuery, refreshPage])
    /* useStates untuk keperluan GET daftar semua merk */

    let electronics = getElectronics();
    return (
        <div style={{ padding: '15px 0' }}>
            <Typography style={{ textTransform: 'capitalize', paddingBottom: "10px" }} >
                <h3 style={{ color: '#4F4F4F', margin: '0' }}>Merek Elektronik</h3>
            </Typography>

            <Carousel
                swipeable={true}
                itemClass="carousel-item-padding-40-px"
                draggable={true}
                responsive={landingPageCategoriesResponsive}
                infinite={true}
                autoPlay={true}
                autoPlaySpeed={3000}
                arrows={false}
                // removeArrowOnDeviceType={'mobile'}
                // centerMode={true}
                // style={{ }}
            >
                {listOfBrands.map((invoice) => (
                    // <Card style={{ height: '' }} component={Link} to={`/category/${invoice.id}`}   >
                    //     <CardMedia component="img" image={`data:image/jpeg;base64,${invoice.image}`} alt={invoice['image']} 
                    //         style={{
                    //             objectFit: 'contain',
                    //             border: '1px solid hsl(1,1%,80%)',
                    //             width: '90%',
                    //             height: '90%',
                    //             borderRadius: '5%',
                    //             padding: '5px'
                    //         }} />
                    // </Card>
                    <Box component={Link} to={`/category/${invoice.id}`}
                        style={{
                            width: '220px', height: '170px',
                            display: 'flex', justifyContent: 'center', alignItems: 'center',
                            border: "1px solid #828282", borderRadius: "10px",
                            marginLeft: "auto", marginRight: "auto"
                        }}
                    >
                        <img src={`data:image/jpeg;base64,${invoice.image}`} alt={invoice['image']} width="170px" hight="120px" />
                    </Box>
                ))}
            </Carousel>
        </div>
    )
}