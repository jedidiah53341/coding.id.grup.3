# Coding.id grup 3
Oleh Dika Irzian, Aji Kumara, dan Jedidiah

#### Ketika pertama kali download ke local :
- jalankan 'git clone https://gitlab.com/jedidiah53341/coding.id.grup.3.git '.
- pada terminal, masuk ke folder project.
- ' npm install '
- ' npm run start '

#### Untuk commit dan push perkembangan :
- cek branch terkini dengan 'git branch', bila blum ada branch pribadi, buat dengan 'git checkout -b <i>nama-branch-pribadi</i>' 
- Untuk memudahkan penambahan banyaknya file yang kita buat, umumnya kita jalankan terminal di root folder project kita, kemudian 'git add .' .
- jalankan 'git commit -m '<i>pesan commit</i>' .
- jalankan 'git push origin <i>nama-branch-pribadi</i>' .

#### Mengenai project
- Setiap routes terdefinisi di file 'index.js' .

- Setiap template yang diminta di tiap routes disimpan dalam file pada folder '/routes/, kecuali template 'App.js' yang berisi template yang menampilkan AppBar dan Footer, yang akan tetap ditampilkan pada route manapun.

- Dalam upaya merapihkan isi setiap file template, maka bagian - bagian dalam template dapat disimpan dalam file tersendiri yang disimpan pada folder '/components/'. Seperti contoh dalam file 'App.js', komponen seperti AppBar dan Footer dapat disimpan ke file tersendiri menjadi '/components/WebsiteAppBar.js' dan '/components/WebsiteFooter.js/' . Hal ini membuat file 'App.js' lebih ringkas dan mudah dibayangkan.

- Untuk sekarang, semua data dinamis di-hardcode kedalam sebuah file, yaitu '/jsonData/Data.js' .
